#ifndef HYDRO_H
#define HYDRO_H

#include <blitz/array.h>
#include <sys/time.h>

#include "parallel.h"
#include "writer.h"
#include "sfield.h"
#include "vfield.h"
#include "reader.h"
#include "grid.h"

class hydro {
    protected:
        int tStepCnt;
        int maxCount;

        double hx, hy, hz;

        double time;
        double maxValue;

        const grid &mesh;
        const reader &inputData;

        parallel &mpiData;

        blitz::Range xCoreRange, yCoreRange, zCoreRange;

        writer *dataWriter;

        sfield Pp;
        sfield mgRHS;

        vfield *H;

        vfield jacGuess;
        vfield lapField;
        vfield gradPres;

        virtual void uSolve();
        virtual void vSolve();
        virtual void wSolve();

        virtual void setCoefficients();

        virtual void implicitCN();

    public:
        sfield *P;

        vfield *V;

        hydro(const grid &mesh, const reader &solParam, parallel &mpiParam);

        virtual void solvePDE();

        virtual ~hydro() { };
};

/**
 ********************************************************************************************************************************************
 *  \class hydro hydro.h "lib/hydro.h"
 *  \brief The base class hydro to solve the incompressible Navier-Stokes equations
 *
 *  The class initializes and stores the velocity vector field and the pressure scalar field along with a few auxilliary
 *  fields to solve the PDE.
 *  It solves the NSE using the \ref solvePDE function from within which the implicit Crank-Nicholson method is used
 *  to solve the PDE.
 ********************************************************************************************************************************************
 */

class hydro2: public hydro {
    private:
        double hx2, hz2, hzhx;

        void uSolve();
        void wSolve();

        void imposeUBCs();
        void imposeWBCs();

        void setCoefficients();

        void implicitCN();

    public:
        hydro2(const grid &mesh, const reader &solParam, parallel &mpiParam);

        void solvePDE();

        ~hydro2();
};

/**
 ********************************************************************************************************************************************
 *  \class hydro2 hydro.h "lib/hydro.h"
 *  \brief The derived class from the hydro base class to solve the incompressible NSE in 2D
 *
 *  Certain paramters to be used in the implicit calculation of velocity are defined separately from within the class.
 *  Since the class is instantiated when solveing the NSE in 2D, the y-direction component of the grid is supressed.
 *  Consequently, the boundary conditions are imposed only on 4 sides of the domain.
 ********************************************************************************************************************************************
 */

class hydro3: public hydro {
    private:
        double hxhy, hyhz, hzhx, hxhyhz;

        void uSolve();
        void vSolve();
        void wSolve();

        void imposeUBCs();
        void imposeVBCs();
        void imposeWBCs();

        void setCoefficients();

        void implicitCN();

    public:
        hydro3(const grid &mesh, const reader &solParam, parallel &mpiParam);

        void solvePDE();

        ~hydro3();
};

/**
 ********************************************************************************************************************************************
 *  \class hydro3 hydro.h "lib/hydro.h"
 *  \brief The derived class from the hydro base class to solve the incompressible NSE in 3D
 *
 *  Certain paramters to be used in the implicit calculation of velocity are defined separately from within the class.
 *  Moreover, it imposes boundary conditions on all the three faces of the computational domain.
 ********************************************************************************************************************************************
 */

#endif
