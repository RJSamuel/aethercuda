#ifndef SFIELD_H
#define SFIELD_H

#include <blitz/array.h>
#include <string>

#include "mpidata.h"
#include "field.h"
#include "grid.h"

class vfield;       // FORWARD DECLARATION

class sfield {
    private:
        blitz::Array<double, 1> x_Metric, y_Metric, z_Metric;
        blitz::Array<double, 1> xxMetric, yyMetric, zzMetric;
        blitz::Array<double, 1> x2Metric, y2Metric, z2Metric;

    public:
        mpidata *mpiHandle;

        std::string fieldName;

        const grid &gridData;

        const bool xStag, yStag, zStag;

        field F;

        blitz::Array<double, 3> dF_dx, dF_dy, dF_dz;
        blitz::Array<double, 3> d2F_dx2, d2F_dy2, d2F_dz2;
        blitz::Array<double, 3> interVx, interVy, interVz;

        blitz::Array<blitz::RectDomain<3>, 1> VxIntSlices, VyIntSlices, VzIntSlices;

        sfield(const grid &gridData, std::string fieldName, bool xStag, bool yStag, bool zStag);

        void calcDerivatives1();
        void calcDerivatives2();

        void setInterpolationSlices();

        void gradient(vfield &gradF);

        void convectiveDerivative(const vfield &V, sfield &H);

        double fieldMax();

        sfield& operator += (sfield &a);
        sfield& operator -= (sfield &a);
        sfield& operator *= (double a);

        ~sfield() { };
};

/**
 ********************************************************************************************************************************************
 *  \class sfield sfield.h "lib/sfield.h"
 *  \brief Scalar field class to store and operate on scalar fields
 *
 *  The class stores scalar fields in the form of an instance of the field class defined in <B>field.h</B>.
 *  While the class <B>field</B> merely stores data in the form of a blitz array and offers functions to compute derivatives
 *  over a uniform grid, the <B>sfield</B> class adds another layer of functionality along with the <B>grid</B> (<B>grid.h</B>)
 *  class to apply necessary grid transformation metrics and compute derivatives over a non-uniform grid.
 *  The scalar field is also equipped with a gradient operator, which returns a vector field (vfield).
 *  However, this operation is presently restricted to cell-centered scalar fields, i.e., those which are staggered in all
 *  the directions.
 *  Moreover, the \f$ (\mathbf{u}.\nabla)f \f$ operator is also provided as the function <B>convectiveDerivative</B>
 ********************************************************************************************************************************************
 */

#endif
