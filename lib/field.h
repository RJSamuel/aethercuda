#ifndef FIELD_H
#define FIELD_H

#include <blitz/array.h>
#include "mpidata.h"
#include "differ.h"
#include "grid.h"

class field {
    private:
        const grid &gridData;

        void setCoreSlice(const bool xStag, const bool yStag, const bool zStag);
        void setBulkSlice(const bool xStag, const bool yStag, const bool zStag);

        void setWallSlices();

    public:
        blitz::Array<double, 3> F;

        blitz::RectDomain<3> fCore;
        blitz::RectDomain<3> fCLft, fCRgt;
        blitz::RectDomain<3> fCFrt, fCBak;
        blitz::RectDomain<3> fCBot, fCTop;

        blitz::RectDomain<3> fBulk;
        blitz::RectDomain<3> fBLft, fBRgt;
        blitz::RectDomain<3> fBFrt, fBBak;
        blitz::RectDomain<3> fBBot, fBTop;

        blitz::Array<blitz::RectDomain<3>, 1> fWalls;

        blitz::TinyVector<int, 3> fSize;

        blitz::TinyVector<int, 3> flBound, cuBound;

        differ *xDeriv;
        differ *yDeriv;
        differ *zDeriv;

        field(const grid &gridData, const bool xStag, const bool yStag, const bool zStag);

        void x1Deriv(blitz::Array<double, 3> dF);
        void y1Deriv(blitz::Array<double, 3> dF);
        void z1Deriv(blitz::Array<double, 3> dF);

        void x2Deriv(blitz::Array<double, 3> dF);
        void y2Deriv(blitz::Array<double, 3> dF);
        void z2Deriv(blitz::Array<double, 3> dF);

        ~field();
};

/**
 ********************************************************************************************************************************************
 *  \class field field.h "lib/field.h"
 *  \brief Field class to store data and perform finite difference operations on the data
 *
 *  The class stores the base data of both scalar and vector fields as blitz arrays.
 *  The data is stored with a uniform grid spacing as in the transformed plane.
 *  Correspondingly, the finite difference operations are also performed with constant grid spacing.
 *  The limits of the full domain and its core are also stored in a set of RectDomain and TinyVector objects.
 ********************************************************************************************************************************************
 */

#endif
