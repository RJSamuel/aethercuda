#ifndef GRID_H
#define GRID_H

#include <math.h>
#include <string>
#include <vector>
#include <blitz/array.h>

#include "reader.h"
#include "parallel.h"

class grid {
    private:
        /** Grid stretching parameter for tangent-hyperbolic function along x direction */
        double xBeta;
        /** Grid stretching parameter for tangent-hyperbolic function along y direction */
        double yBeta;
        /** Grid stretching parameter for tangent-hyperbolic function along z direction */
        double zBeta;

        /** Array of the local values of \f$ \xi \f$ within the MPI-decomposed sub-domains in the transformed plane */
        blitz::Array<double, 1> xi;
        /** Array of the local values of \f$ \eta \f$ within the MPI-decomposed sub-domains in the transformed plane */
        blitz::Array<double, 1> et;
        /** Array of the local values of \f$ \zeta \f$ within the MPI-decomposed sub-domains in the transformed plane */
        blitz::Array<double, 1> zt;

        /** Array of the global values of \f$ \xi \f$ within the full domain in the transformed plane */
        blitz::Array<double, 1> xiGlo;
        /** Array of the global values of \f$ \eta \f$ within the full domain in the transformed plane */
        blitz::Array<double, 1> etGlo;
        /** Array of the global values of \f$ \zeta \f$ within the full domain in the transformed plane */
        blitz::Array<double, 1> ztGlo;

        void resizeGrid();
        void makeSizeArray();
        void setDomainSizes();
        void globalXiEtaZeta();

        void createTanHypGrid();
        void createUniformGrid();

        void computeGlobalLimits();

    public:
        /** A const reference to the global variables stored in the reader class to access user set parameters */
        const reader &inputData;

        /** A const reference to the global variables stored in the parallel class to access MPI related parameters */
        const parallel &rankData;

        /** The sizes of the core of MPI decomposed sub-domains without the pads (collocated points) - localNz, localNy, localNx */
        blitz::TinyVector<int, 3> collocCoreSize;

        /** The sizes of the MPI decomposed sub-domains including the pads on all sides (collocated points) - collocCoreSize + 2*padWidths */
        blitz::TinyVector<int, 3> collocFullSize;

        /** The sizes of the core of MPI decomposed sub-domains without the pads (staggered points) */
        blitz::TinyVector<int, 3> staggrCoreSize;

        /** The sizes of the MPI decomposed sub-domains including the pads on all sides (staggered points) - staggrCoreSize + 2*padWidths */
        blitz::TinyVector<int, 3> staggrFullSize;

        /** The sizes of the pad widths along the three directions - padZ, padY, padX */
        blitz::TinyVector<int, 3> padWidths;

        /** The size of the entire computational domain excluding the pads at the boundary of full domain - globalNz, globalNy, globalNx */
        blitz::TinyVector<int, 3> globalSize;

        /** The end indices of the MPI decomposed sub-domains within the global indexing of the full computational domain - ztEn, etEn, xiEn */
        blitz::TinyVector<int, 3> subarrayEnds;

        /** The start indices of the MPI decomposed sub-domains within the global indexing of the full computational domain - ztSt, etSt, xiSt */
        blitz::TinyVector<int, 3> subarrayStarts;

        /** Grid spacing in the transformed plane along the \f$ \xi \f$ direction */
        double dXi;

        /** Grid spacing in the transformed plane along the \f$ \eta \f$ direction */
        double dEt;

        /** Grid spacing in the transformed plane along the \f$ \zeta \f$ direction */
        double dZt;

        /** Length of the physical computational domain along the x direction */
        double xLen;

        /** Length of the physical computational domain along the y direction */
        double yLen;

        /** Length of the physical computational domain along the z direction */
        double zLen;

        /** Array of collocated grid sizes such that the corresponding staggered grid will be multi-grid compatible */
        blitz::Array<int, 1> sizeArray;

        /** Vector of indices pointing to the <B>sizeArray</B> that determines the global full domain size along the 3 directions */
        blitz::TinyVector<int, 3> sizeIndex;

        /** RectDomain object that defines the slice for the core of the local MPI decomposed sub-domain (collocated points) */
        blitz::RectDomain<3> collocCoreDomain;

        /** RectDomain object that defines the slice for the full extent of the local MPI decomposed sub-domain (collocated points) */
        blitz::RectDomain<3> collocFullDomain;

        /** RectDomain object that defines the slice for the core of the local MPI decomposed sub-domain (staggered points) */
        blitz::RectDomain<3> staggrCoreDomain;

        /** RectDomain object that defines the slice for the full extent of the local MPI decomposed sub-domain (staggered points) */
        blitz::RectDomain<3> staggrFullDomain;

        /** Collocated grid along the x-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> xColloc;

        /** Collocated grid along the y-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> yColloc;

        /** Collocated grid along the z-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> zColloc;

        /** Staggered grid along the x-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> xStaggr;

        /** Staggered grid along the y-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> yStaggr;

        /** Staggered grid along the z-direction defined locally within MPI decomposed sub-domains */
        blitz::Array<double, 1> zStaggr;

        /*****************************************************************************************************************************************************/

        /** Array of the grid derivatives \f$ \frac{\partial\xi}{\partial x} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xi_xColloc;

        /** Array of the grid derivatives \f$ \frac{\partial\xi}{\partial x} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xi_xStaggr;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \xi}{\partial x^2} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xixxColloc;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \xi}{\partial x^2} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xixxStaggr;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\xi}{\partial x}\right)^2 \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xix2Colloc;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\xi}{\partial x}\right)^2 \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> xix2Staggr;

        /*****************************************************************************************************************************************************/

        /** Array of the grid derivatives \f$ \frac{\partial\eta}{\partial y} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> et_yColloc;

        /** Array of the grid derivatives \f$ \frac{\partial\eta}{\partial y} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> et_yStaggr;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \eta}{\partial y^2} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> etyyColloc;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \eta}{\partial y^2} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> etyyStaggr;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\eta}{\partial y}\right)^2 \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ety2Colloc;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\eta}{\partial y}\right)^2 \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ety2Staggr;

        /*****************************************************************************************************************************************************/

        /** Array of the grid derivatives \f$ \frac{\partial\zeta}{\partial z} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> zt_zColloc;

        /** Array of the grid derivatives \f$ \frac{\partial\zeta}{\partial z} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> zt_zStaggr;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \zeta}{\partial z^2} \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ztzzColloc;

        /** Array of the grid derivatives \f$ \frac{\partial^2 \zeta}{\partial z^2} \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ztzzStaggr;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\zeta}{\partial z}\right)^2 \f$ at collocated grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ztz2Colloc;

        /** Array of the grid derivatives \f$ \left(\frac{\partial\zeta}{\partial z}\right)^2 \f$ at staggered grid points, defined locally within each sub-domain. */
        blitz::Array<double, 1> ztz2Staggr;

        grid(const reader &solParam, parallel &parallelData);
};

/**
 ********************************************************************************************************************************************
 *  \class grid grid.h "lib/grid.h"
 *  \brief  Contains all the global variables related to the grid, its slices, limits, and grid derivatives used
 *          throughout the solver
 ********************************************************************************************************************************************
 */

#endif
