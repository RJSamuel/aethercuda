#ifndef WRITER_H
#define WRITER_H

#include <blitz/array.h>
#include <fstream>
#include <iomanip>
#include <sstream>
#include "sfield.h"
#include "vfield.h"
#include "grid.h"
#include "hdf5.h"

class writer {
    private:
        const grid &mesh;
        const vfield &velocity;
        const sfield &pressure;

        int tabWidth, fltWidth, dataPrec, lineWidth;

        char *dataField;

        blitz::Array<double, 1> x, y, z;

#ifdef PLANAR
        blitz::Array<double, 2> fieldData;
#else
        blitz::Array<double, 3> fieldData;
#endif

        MPI_Datatype STRING_DATA;
        MPI_Datatype DATA_ARRAY;

        blitz::TinyVector<int, 3> gloSize, locSize, sdStart;

    public:
        writer(const grid &mesh, const vfield &velocity, const sfield &pressure);

        void initLimits();
        void initMPIIO();

        void copyData(const sfield &outField);

        void writeHDF5(double time);
        void writeASCII(double time);

        ~writer();
};

/**
 ********************************************************************************************************************************************
 *  \class writer writer.h "lib/writer.h"
 *  \brief Class for all the global variables and functions related to writing output data of the solver.
 *
 *  The computational data from the solver is written in ASCII format in a .dat file.
 *  The data is written to be compatible with Tecplot and hence writes the corresponding headers in the solution files.
 *  MPI-IO is used to write the data in parallel, and hence all the associated MPI derived data-types are stored in this class.
 *  The class allows for both collocated and staggered grid data to be written in separate output files.
 ********************************************************************************************************************************************
 */

#endif
