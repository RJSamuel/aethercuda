\documentclass[12pt]{article}

\usepackage{url}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[parfill]{parskip}
\usepackage[margin=1.0in]{geometry}

\begin{document}
\title{Aether \\
       \large An MPI Parallel Finite Difference PDE Solver}
\author{Roshan Samuel}
\maketitle

\section{Introducion}
The Aether solver computes solutions to the Navier-Stokes equations using user specified boundary conditions.
The code of the solver is arranged in the following directories:
\begin{enumerate}
    \item \texttt{lib} directory contains the header files which declare the functions used in the solver
    \item \texttt{src} directory contains the source files which define the functions declared in \texttt{lib} folder
    \item \texttt{output} directory is where the solver writes the output files to, and is empty initially
    \item \texttt{install} directory contains the install scripts and the \texttt{CMakeLists} file which is used by \texttt{cmake} to compile the solver
\end{enumerate}

The various parameters necessary to setup the run of the solver are defined in the \path{parameters.yaml} file found in the root directory of the solver.

The various classes of the solver are:
\begin{enumerate}
    \item \texttt{differ}: contains blitz operations to perform finite differencing operations
    \item \texttt{field}: the base class that contains a 3D blitz array to hold field data
    \item \texttt{sfield}: scalar field which is derived from the \texttt{field} class
    \item \texttt{vfield}: vector field which is defined using three \texttt{sfield} objects
    \item \texttt{grid}: all data relating to grid coordinates, spacing etc are stored here
    \item \texttt{hydro}: base class for \texttt{hydro2} and \texttt{hydro3} containing functions to solve NSE
    \item \texttt{mpidata}: class attached to \texttt{field} class to synchronize data with MPI derived datatypes
    \item \texttt{parallel}: all MPI related data like rank, neighbours rank etc are stored here
    \item \texttt{reader}: this class contains functions to parse \texttt{parameters.yaml} and store constants
    \item \texttt{writer}: writing of solution data in HDF5 format is done by an instance of this class
\end{enumerate}

For installing and running the solver, the following libraries will have to be installed.
\begin{enumerate}
    \item \texttt{blitz} library is used for array manipulations and can be downloaded from \href{https://sourceforge.net/projects/blitz/}{here}.
    \item \texttt{cmake} is used for installing the solver and can normally be installed using the available package manager in Linux.
    \item \texttt{yaml-cpp} is used to parse user input and can be either installed through package manager or downloaded from \href{https://github.com/jbeder/yaml-cpp}{here}.
\end{enumerate}

\section{Governing Equations}

The Navier Stokes equations can be written as:
\begin{equation}
    \frac{\partial\mathbf{u}}{\partial t} + \mathbf{u}.\nabla\mathbf{u} = -\frac{\nabla p}{\rho} + \nu\nabla^2\mathbf{u}
\end{equation}

In the present version of the solver for CUDA parallelization, the pressure gradient terms are ignored, yielding:
\begin{equation}
    \frac{\partial\mathbf{u}}{\partial t} + \mathbf{u}.\nabla\mathbf{u} = \nu\nabla^2\mathbf{u}
\end{equation}

Using \(\mathbf{u}^{(n)}\) to indicate the velocity field at time \(t = n\), and after non-dimensionalization, the above PDE can be discretized for Euler time-integration as below:
\begin{equation}
    \frac{\mathbf{u}^{(n+1)} - \mathbf{u}^{(n)}}{\delta t} = \frac{\nabla^2\mathbf{u}^{(n)}}{Re} - \mathbf{u}^{(n)}.\nabla\mathbf{u}^{(n)}
\end{equation}
\begin{equation}
    \mathbf{u}^{(n+1)} = \mathbf{u}^{(n)} + \delta t\left(\frac{\nabla^2\mathbf{u}^{(n)}}{Re} - \mathbf{u}^{(n)}.\nabla\mathbf{u}^{(n)}\right)
\end{equation}

In the above equation, the viscous diffusion term (the second term in the RHS) is computed using the function \texttt{computeViscous} within the vector field class - \texttt{vfield}.
The non-linear term (the last term in the RHS) is computed using the function \texttt{computeNLin}, again in \texttt{vfield},
which in turn calls the function \texttt{convectiveDerivative}, of the scalar field class - \texttt{sfield}.

The solver presently uses semi-implicit Crank-Nicholson method.
In this method, the viscous term is split into two, with part contribution from the present time-step and the other part from the next time-step:
\begin{equation}
    \mathbf{u}^{(n+1)} = \mathbf{u}^{(n)} + \delta t\left(\frac{\nabla^2\mathbf{u}^{(n)} + \nabla^2\mathbf{u}^{(n+1)}}{2Re} - \mathbf{u}^{(n)}.\nabla\mathbf{u}^{(n)}\right)
\end{equation}
\begin{equation}
    \mathbf{u}^{(n+1)} - \frac{\delta t\nabla^2\mathbf{u}^{(n+1)}}{2Re} = \mathbf{u}^{(n)} + \delta t\left(\frac{\nabla^2\mathbf{u}^{(n)}}{2Re} - \mathbf{u}^{(n)}.\nabla\mathbf{u}^{(n)}\right)
\end{equation}

This is an implicit equation that must be solved iteratively.
The solver uses Gauss-Seidel iterations to compute the three components of the velocity field this way.
The corresponding functions are \texttt{uSolve}, \texttt{vSolve} and \texttt{wSolve}, all belonging to the \texttt{hydro} class.

Since the solver computes both 2D and 3D flows, the above functions are written for both 2D and 3D arrays.
This is done using virtual functions of C++ in which the base \texttt{hydro} class is inherited by the \texttt{hydro2} and \texttt{hydro3} classes.

All the above described steps of the semi-implicit Crank-Nicholson method are performed in the function \texttt{implictCN} within the \texttt{hydro} class.
This function is called once in each loop of time-integration, which is written in the \texttt{solvePDE} function of the \texttt{hydro} class.
This function also computes maximum divergence and total energy in the domain at each step of the time-integration loop.
These computations can also be parallelized.

\section{Boundary Conditions}

The solver is currently capable of solving for flows with both no-slip and periodic boundary conditions.
For testing no-slip BCs, the test case used is the lid-driven cavity problem, while for testing periodic BCs, the decay of Taylor-Green vortices can be simulated.
The flow configuration and appropriate BCs are set using the parameter \texttt{Problem Type} in the \texttt{parameters.yaml} file.
The BCs appropriate to each problem are set in the functions \texttt{imposeUBCs}, \texttt{imposeVBCs} and \texttt{imposeWBCs} of the \texttt{hydro} class.

\section{Summary}

With the pressure Poisson solver removed from the solver, the computation intensive functions that can be focussed on for parallelization are:
\begin{enumerate}
    \item \texttt{computeViscous} function of \texttt{vfield} class
    \item \texttt{computeNLin} function of \texttt{vfield} class
    \item \texttt{uSolve} function of \texttt{hydro} class
    \item \texttt{vSolve} function of \texttt{hydro} class
    \item \texttt{wSolve} function of \texttt{hydro} class
\end{enumerate}

The computation of maximum divergence and total energy at each time-step within the \texttt{solvePDE} function of the \texttt{hydro} class can also be parallelized
for additional performance benefits.

Presently, the solver uses the \texttt{blitz} library for array manipulations.
As a result, the first 2 functions listed above have very few lines of code.
These functions therefore have to be re-written in terms of base C++ array manipulations and then parallelized using CUDA arrays.
\end{document}
