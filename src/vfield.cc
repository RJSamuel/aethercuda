#include "vfield.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the vfield class
 *
 *          Three instances of the sfield class to store the data of the three component scalar fields are initialized.
 *          The scalar fields are initialized with appropriate grid staggering to place the components on the cell faces.
 *          The name for the vector field as given by the user is also assigned.
 *
 * \param   gridData is a const reference to the global data contained in the grid class
 * \param   fieldName is a string value set by the user to name and identify the vector field
 ********************************************************************************************************************************************
 */
vfield::vfield(const grid &gridData, std::string fieldName):
               gridData(gridData),
               Vx(gridData, "Vx", false, true, true),
               Vy(gridData, "Vy", true, false, true),
               Vz(gridData, "Vz", true, true, false)
{
    this->fieldName = fieldName;

}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the viscous term of NSE
 *
 *          It is assumed that the velocity is specified at face-centers, as required by the \ref sfield#convectiveDerivative
 *          "convectiveDerivative" function of sfield.
 *
 * \param   H is a pointer to a vector field (vfield) to which the output of the function is to be written
 ********************************************************************************************************************************************
 */
void vfield::computeViscous(vfield *H) {
    calcDerivatives2();

#ifdef PLANAR
    H->Vx.F.F = Vx.d2F_dx2 + Vx.d2F_dz2;
    H->Vy.F.F = Vy.d2F_dx2 + Vy.d2F_dz2;
    H->Vz.F.F = Vz.d2F_dx2 + Vz.d2F_dz2;
#else
    H->Vx.F.F = Vx.d2F_dx2 + Vx.d2F_dy2 + Vx.d2F_dz2;
    H->Vy.F.F = Vy.d2F_dx2 + Vy.d2F_dy2 + Vy.d2F_dz2;
    H->Vz.F.F = Vz.d2F_dx2 + Vz.d2F_dy2 + Vz.d2F_dz2;
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the convective derivative of the vector field
 *
 *          The function computes for the operator \f$ (\mathbf{u}.\nabla)\mathbf{v} \f$ for the vector field, \f$\mathbf{v}\f$.
 *          To do so, the function needs the vector field (vfield) of velocity, \f$\mathbf{u}\f$.
 *          This value is used in three separate calls to the \ref sfield#convectiveDerivative "convectiveDerivative" function of sfield
 *          to compute the derivatives for the three components of the vector field.
 *          It is assumed that the velocity is specified at face-centers, as required by the \ref sfield#convectiveDerivative
 *          "convectiveDerivative" function of sfield.
 *
 * \param   V is a const reference to a vector field (vfield) that specifies the convection velocity at each point
 * \param   H is a pointer to a vector field (vfield) to which the output of the function is to be written
 ********************************************************************************************************************************************
 */
void vfield::computeNLin(const vfield &V, vfield *H) {
    Vx.convectiveDerivative(V, H->Vx);
    Vy.convectiveDerivative(V, H->Vy);
    Vz.convectiveDerivative(V, H->Vz);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the first derivatives of all the component scalar fields in the vector field
 *
 *          The first derivatives of all the three components of the vector field is computed by calling the
 *          \ref sfield#calcDerivatives1 "calcDerivatives1" function of each of the individual scalar fields (sfield) - Vx, Vy and Vz.
 ********************************************************************************************************************************************
 */
void vfield::calcDerivatives1() {
    Vx.calcDerivatives1();
    Vy.calcDerivatives1();
    Vz.calcDerivatives1();
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the second derivatives of all the component scalar fields in the vector field
 *
 *          The second derivatives of all the three components of the vector field is computed by calling the
 *          \ref sfield#calcDerivatives2 "calcDerivatives2" function of each of the individual scalar fields (sfield) - Vx, Vy and Vz.
 ********************************************************************************************************************************************
 */
void vfield::calcDerivatives2() {
    Vx.calcDerivatives2();
    Vy.calcDerivatives2();
    Vz.calcDerivatives2();
}

/**
 ********************************************************************************************************************************************
 * \brief   Operator to compute the divergence of the vector field
 *
 *          The operator computes the divergence of a face-centered staggered vector field, and stores it into a cell centered
 *          scalar field as defined by the tensor operation:
 *          \f$ \nabla . \mathbf{v} = \frac{\partial \mathbf{v}}{\partial x} +
 *                                    \frac{\partial \mathbf{v}}{\partial y} +
 *                                    \frac{\partial \mathbf{v}}{\partial z} \f$.
 *
 * \param   divV is a pointer to a scalar field (sfield) into which the computed divergence must be written.
 ********************************************************************************************************************************************
 */
void vfield::divergence(sfield *divV) {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    blitz::Range xStag, yStag, zStag;

    xStag = blitz::Range(divV->F.fCore.lbound(0), divV->F.fCore.ubound(0));
    yStag = blitz::Range(divV->F.fCore.lbound(1), divV->F.fCore.ubound(1));
    zStag = blitz::Range(divV->F.fCore.lbound(2), divV->F.fCore.ubound(2));

    divV->F.F = 0.0;

#ifdef PLANAR
    divV->F.F(divV->F.fCore) = gridData.xi_xStaggr(xStag)(i)*(Vx.F.F(divV->F.fCore) - Vx.F.F(divV->F.fCLft))/gridData.dXi + 
                               gridData.zt_zStaggr(zStag)(k)*(Vz.F.F(divV->F.fCore) - Vz.F.F(divV->F.fCBot))/gridData.dZt;
#else
    divV->F.F(divV->F.fCore) = gridData.xi_xStaggr(xStag)(i)*(Vx.F.F(divV->F.fCore) - Vx.F.F(divV->F.fCLft))/gridData.dXi + 
                               gridData.et_yStaggr(yStag)(j)*(Vy.F.F(divV->F.fCore) - Vy.F.F(divV->F.fCFrt))/gridData.dEt + 
                               gridData.zt_zStaggr(zStag)(k)*(Vz.F.F(divV->F.fCore) - Vz.F.F(divV->F.fCBot))/gridData.dZt;
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to synchronise data across all processors when performing parallel computations
 *
 *          Each of the individual scalar field components have their own subroutine, \ref mpidata#syncData "syncData" to send and
 *          receive data across its MPI decomposed sub-domains.
 *          This function calls the \ref mpidata#syncData "syncData" function of its components to perform perform data-transfer and thus update
 *          the sub-domain boundary pads.
 ********************************************************************************************************************************************
 */
void vfield::syncData() {
    Vx.mpiHandle->syncData();
    Vy.mpiHandle->syncData();
    Vz.mpiHandle->syncData();
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to add a given vector field
 *
 *          The unary operator += adds a given vector field to the entire field stored as vfield and returns
 *          a pointer to itself.
 *
 * \param   a is a reference to another vfield to be deducted from the member fields
 *
 * \return  A pointer to itself is returned by the vector field class to which the operator belongs
 ********************************************************************************************************************************************
 */
vfield& vfield::operator += (vfield &a) {
    Vx += a.Vx;
    Vy += a.Vy;
    Vz += a.Vz;

    return *this;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to subtract a given vector field
 *
 *          The unary operator -= subtracts a given vector field from the entire field stored as vfield and returns
 *          a pointer to itself.
 *
 * \param   a is a reference to another vfield to be deducted from the member fields
 *
 * \return  A pointer to itself is returned by the vector field class to which the operator belongs
 ********************************************************************************************************************************************
 */
vfield& vfield::operator -= (vfield &a) {
    Vx -= a.Vx;
    Vy -= a.Vy;
    Vz -= a.Vz;

    return *this;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to multiply a scalar value to the vector field
 *
 *          The unary operator *= multiplies a double precision value to all the fields (Vx, Vy and Vz) stored in vfield and returns
 *          a pointer to itself.
 *
 * \param   a is a double precision number to be multiplied to the vector field
 *
 * \return  A pointer to itself is returned by the vector field class to which the operator belongs
 ********************************************************************************************************************************************
 */
vfield& vfield::operator *= (double a) {
    Vx *= a;
    Vy *= a;
    Vz *= a;

    return *this;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to assign a scalar value to the vector field
 *
 *          The operator = assigns a double precision value to all the fields (Vx, Vy and Vz) stored in vfield.
 *
 * \param   a is a double precision number to be assigned to the vector field
 ********************************************************************************************************************************************
 */
void vfield::operator = (double a) {
    Vx.F.F = a;
    Vy.F.F = a;
    Vz.F.F = a;
}

/**
 ********************************************************************************************************************************************
 * \brief   Brief description of function
 *
 *          Detailed description of function with paragraphs, formulae etc. Use LaTeX equation formulae within the delimiters
 *          as given below:
 *          \f$ 2^N + 1 \f$
 *
 * \param   paramName Input parameters of the function 
 *
 * \return  Description about the return value from the function
 ********************************************************************************************************************************************
 */
