#include <iostream>
#include "parallel.h"
#include "reader.h"
#include "hydro.h"
#include "grid.h"

int main() {
    struct timeval runStart, runEnd;

    // INITIALIZE MPI
    MPI_Init(NULL, NULL);

    // ALL PROCESSES READ THE INPUT PARAMETERS
    reader inputData;

    // INITIALIZE PARALLELIZATION DATA
    parallel mpi(inputData);

    grid gridData(inputData, mpi);
    hydro *nseSolver;

    // CREATE NEW INSTANCE OF THE HYDRODYNAMICS SOLVER
#ifdef PLANAR
    nseSolver = new hydro2(gridData, inputData, mpi);
#else
    nseSolver = new hydro3(gridData, inputData, mpi);
#endif

    gettimeofday(&runStart, NULL);

    nseSolver->solvePDE();

    gettimeofday(&runEnd, NULL);
    double run_time = ((runEnd.tv_sec - runStart.tv_sec)*1000000u + runEnd.tv_usec - runStart.tv_usec)/1.e6;

    if (mpi.rank == 0) {
        std::cout << "\033[32mSimulation completed\033[0m" << std::endl;
        std::cout << std::endl;
        std::cout << "Time taken by simulation: " << run_time << std::endl;
    }

    delete nseSolver;

    // FINALIZE AND CLEAN-UP
    MPI_Finalize();

    return 0;
}
