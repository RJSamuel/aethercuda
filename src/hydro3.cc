#include "hydro.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the hydro3 class derived from the base hydro class
 *
 *          The constructor passes its arguments to the base hydro class and then initializes all the scalar and
 *          vector fields necessary for solving the NS equations.
 *          The various coefficients for solving the equations are also set by a call to the \ref setCoefficients function.
 *          Based on the problem type specified by the user in the parameters file, and stored by the \ref reader class as
 *          \ref reader#probType "probType", the appropriate initial and boundary conditions are specified.
 *
 * \param   mesh is a const reference to the global data contained in the grid class
 * \param   solParam is a const reference to the user-set parameters contained in the reader class
 ********************************************************************************************************************************************
 */
hydro3::hydro3(const grid &mesh, const reader &solParam, parallel &mpiParam): hydro(mesh, solParam, mpiParam) {
    P = new sfield(mesh, "P", true, true, true);
    V = new vfield(mesh, "V");

    H = new vfield(mesh, "H");

    // CREATE NEW INSTANCE OF THE WRITER OBJECT USING THE GRID
    dataWriter = new writer(mesh, *V, *P);

    // SET VALUES OF COEFFICIENTS USED FOR COMPUTING LAPLACIAN
    setCoefficients();

    // INITIALIZE VARIABLES
    if (inputData.probType == "LDC") {
        // INITIALIZE PRESSURE TO 1.0 THROUGHOUT THE DOMAIN
        P->F.F = 1.0;

        // FOR LDC, SET THE X-VELOCITY OF STAGGERED POINTS ON THE LID TO 1.0
        V->Vx.F.F(blitz::Range::all(), blitz::Range::all(), mesh.staggrCoreDomain.ubound(2)) = 1.0;

        // Disable periodic data transfer by setting neighbouring ranks of boundary sub-domains to NULL
        if (mpiData.xRank == 0)             mpiData.nearRanks(0) = MPI_PROC_NULL;
        if (mpiData.xRank == mpiData.npX-1) mpiData.nearRanks(1) = MPI_PROC_NULL;

        if (mpiData.yRank == 0)             mpiData.nearRanks(2) = MPI_PROC_NULL;
        if (mpiData.yRank == mpiData.npY-1) mpiData.nearRanks(3) = MPI_PROC_NULL;

    } else if (inputData.probType == "TGV") {
        // INITIALIZE PRESSURE TO 1.0 THROUGHOUT THE DOMAIN
        P->F.F = 1.0;

        // X-VELOCITY
        for (int i=V->Vx.F.F.lbound(0); i <= V->Vx.F.F.ubound(0); i++) {
            for (int j=V->Vx.F.F.lbound(1); j <= V->Vx.F.F.ubound(1); j++) {
                for (int k=V->Vx.F.F.lbound(2); k <= V->Vx.F.F.ubound(2); k++) {
                    V->Vx.F.F(i, j, k) = sin(2.0*M_PI*mesh.xColloc(i)/mesh.xLen)*
                                         cos(2.0*M_PI*mesh.yStaggr(j)/mesh.yLen)*
                                         cos(2.0*M_PI*mesh.zStaggr(k)/mesh.zLen);
                }
            }
        }

        // Y-VELOCITY
        for (int i=V->Vy.F.F.lbound(0); i <= V->Vy.F.F.ubound(0); i++) {
            for (int j=V->Vy.F.F.lbound(1); j <= V->Vy.F.F.ubound(1); j++) {
                for (int k=V->Vy.F.F.lbound(2); k <= V->Vy.F.F.ubound(2); k++) {
                    V->Vy.F.F(i, j, k) = -cos(2.0*M_PI*mesh.xStaggr(i)/mesh.xLen)*
                                          sin(2.0*M_PI*mesh.yColloc(j)/mesh.yLen)*
                                          cos(2.0*M_PI*mesh.zStaggr(k)/mesh.zLen);
                }
            }
        }

        // Z-VELOCITY
        V->Vz.F.F = 0.0;
    }

    imposeUBCs();
    imposeVBCs();
    imposeWBCs();
}

void hydro3::solvePDE() {
    int xLow, xTop;
    int yLow, yTop;
    int zLow, zTop;

    double dVol;
    double fwTime;
    double totalEnergy, localEnergy;

    int addCount;
    int seriesLength = (int)((inputData.tMax/inputData.tStp)/inputData.ioCnt);

    blitz::Array<double, 1> dSeries, eSeries, tSeries;

    if (mesh.rankData.rank == 0) {
        tSeries.resize(seriesLength + 1);
        dSeries.resize(seriesLength + 1);
        eSeries.resize(seriesLength + 1);

        addCount = 0;
    }

    sfield divV(mesh, "DIV_V", true, true, true);

    // FILE WRITING TIME
    fwTime = 0.0;

    time = 0.0;
    tStepCnt = 0;

    // PARAMETERS FOR COMPUTING TOTAL ENERGY IN THE DOMAIN
    // UPPER AND LOWER LIMITS WHEN COMPUTING ENERGY IN STAGGERED GRID
    xLow = P->F.fCore.lbound(0);        xTop = P->F.fCore.ubound(0);
    yLow = P->F.fCore.lbound(1);        yTop = P->F.fCore.ubound(1);
    zLow = P->F.fCore.lbound(2);        zTop = P->F.fCore.ubound(2);

    // STAGGERED GRIDS HAVE SHARED POINT ACROSS MPI-SUBDOMAINS - ACCORDINGLY DECREASE LIMITS
    if (mesh.rankData.xRank > 0) {
        xLow += 1;
    }

    if (mesh.rankData.yRank > 0) {
        yLow += 1;
    }

    // INFINITESIMAL VOLUME FOR INTEGRATING ENERGY OVER DOMAIN
    dVol = hx*hy*hz;

    // COMPUTE ENERGY AND DIVERGENCE FOR THE INITIAL CONDITION
    V->divergence(&divV);
    maxValue = divV.fieldMax();

    localEnergy = 0.0;
    totalEnergy = 0.0;
    for (int iX = xLow; iX <= xTop; iX++) {
        for (int iY = yLow; iY <= yTop; iY++) {
            for (int iZ = zLow; iZ <= zTop; iZ++) {
                localEnergy += (pow((V->Vx.F.F(iX-1, iY, iZ) + V->Vx.F.F(iX, iY, iZ))/2.0, 2.0) +
                                pow((V->Vy.F.F(iX, iY-1, iZ) + V->Vy.F.F(iX, iY, iZ))/2.0, 2.0) +
                                pow((V->Vz.F.F(iX, iY, iZ-1) + V->Vz.F.F(iX, iY, iZ))/2.0, 2.0))*dVol;
            }
        }
    }

    MPI_Allreduce(&localEnergy, &totalEnergy, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);

    if (mesh.rankData.rank == 0) {
        std::cout << "Time: " << time << std::endl;
        std::cout << "Total energy = " << totalEnergy << std::endl;
        std::cout << "Maximum value of divergence = " << maxValue << std::endl;
        std::cout << std::endl;

        tSeries(addCount) = time;
        eSeries(addCount) = totalEnergy;
        dSeries(addCount) = maxValue;
        addCount += 1;
    }

    // TIME-INTEGRATION LOOP
    while (true) {
        if (std::abs(fwTime - time) < 0.5*inputData.tStp) {
            dataWriter->writeHDF5(time);
            fwTime += inputData.fwInt;
        }

        // MAIN FUNCTION CALLED IN EACH LOOP TO UPDATE THE FIELDS AT EACH TIME-STEP
        implicitCN();

        tStepCnt += 1;
        time += inputData.tStp;

        if (time > inputData.tMax) {
            break;
        }

        V->divergence(&divV);
        maxValue = divV.fieldMax();

        localEnergy = 0.0;
        totalEnergy = 0.0;
        for (int iX = xLow; iX <= xTop; iX++) {
            for (int iY = yLow; iY <= yTop; iY++) {
                for (int iZ = zLow; iZ <= zTop; iZ++) {
                    localEnergy += (pow((V->Vx.F.F(iX-1, iY, iZ) + V->Vx.F.F(iX, iY, iZ))/2.0, 2.0) +
                                    pow((V->Vy.F.F(iX, iY-1, iZ) + V->Vy.F.F(iX, iY, iZ))/2.0, 2.0) +
                                    pow((V->Vz.F.F(iX, iY, iZ-1) + V->Vz.F.F(iX, iY, iZ))/2.0, 2.0))*dVol;
                }
            }
        }

        MPI_Allreduce(&localEnergy, &totalEnergy, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);

        if (tStepCnt % inputData.ioCnt == 0) {
            if (mesh.rankData.rank == 0) {
                std::cout << "Time: " << time << std::endl;
                std::cout << "Total energy = " << totalEnergy << std::endl;
                std::cout << "Maximum value of divergence = " << maxValue << std::endl;
                std::cout << std::endl;

                tSeries(addCount) = time;
                eSeries(addCount) = totalEnergy;
                dSeries(addCount) = maxValue;
                addCount += 1;
            }
        }
    }

    // WRITE THE TIME SERIES OF ENERGY AND DIVERGENCE INTO FILE
    if (mesh.rankData.rank == 0) {
        std::ofstream ofFile("output/TimeSeries.dat");
        ofFile << "VARIABLES = time, energy, divergence\n"
               << "ZONE T=S\tI = " << seriesLength + 1 <<
                          "\tJ = " << 1 <<
                          "\tK = " << 1 <<
                          "\tF = POINT\n";
        for (int i=0; i<=seriesLength; i++) {
            ofFile << std::fixed << std::setw(6)  << std::setprecision(4) << tSeries(i) << "\t" <<
                                    std::setw(16) << std::setprecision(8) << eSeries(i) << "\t" <<
                                                                             dSeries(i) << std::endl;
        }
        ofFile.close();
    }
}

void hydro3::implicitCN() {
    *H = 0.0;

    // CALCULATE H FROM THE NON LINEAR TERMS AND HALF THE VISCOUS TERMS
    V->computeViscous(H);

    // TEMPORARILY WRITING THE CONVECTIVE DERIVATIVE INTO THE gradPres FIELD
    V->computeNLin(*V, &gradPres);
    *H -= gradPres;

    // MULTIPLY WITH TIME-STEP
    *H *= inputData.tStp;

    // ADD THE CALCULATED VALUES TO THE VELOCITY AT START OF TIME-STEP
    *H += *V;

    // SYNCHRONISE THE RHS OF TIME INTEGRATION STEP THUS OBTAINED ACROSS ALL PROCESSORS
    H->syncData();

    // CALCULATE V IMPLICITLY USING THE JACOBI ITERATIVE SOLVER
    uSolve();
    vSolve();
    wSolve();

    if (tStepCnt % inputData.ioCnt == 0) {
        if (mesh.rankData.rank == 0) {
            std::cout << std::endl;
        }
    }

    // IMPOSE BOUNDARY CONDITIONS ON V
    imposeUBCs();
    imposeVBCs();
    imposeWBCs();
}

void hydro3::uSolve() {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    int iterCount = 0;

    xCoreRange = blitz::Range(V->Vx.F.fBulk.lbound(0), V->Vx.F.fBulk.ubound(0));
    yCoreRange = blitz::Range(V->Vx.F.fBulk.lbound(1), V->Vx.F.fBulk.ubound(1));
    zCoreRange = blitz::Range(V->Vx.F.fBulk.lbound(2), V->Vx.F.fBulk.ubound(2));

    while (true) {
        jacGuess.Vx.F.F(V->Vx.F.fBulk) = ((hyhz * mesh.xix2Colloc(xCoreRange)(i) * (V->Vx.F.F(V->Vx.F.fBRgt) + V->Vx.F.F(V->Vx.F.fBLft)) +
                                           hzhx * mesh.ety2Staggr(yCoreRange)(j) * (V->Vx.F.F(V->Vx.F.fBBak) + V->Vx.F.F(V->Vx.F.fBFrt)) +
                                           hxhy * mesh.ztz2Staggr(zCoreRange)(k) * (V->Vx.F.F(V->Vx.F.fBTop) + V->Vx.F.F(V->Vx.F.fBBot))) *
                        inputData.tStp / ( hxhyhz * 2.0 * inputData.Re) + H->Vx.F.F(V->Vx.F.fBulk))/
                 (1.0 + inputData.tStp * ((hyhz * mesh.xix2Colloc(xCoreRange)(i) +
                                           hzhx * mesh.ety2Staggr(yCoreRange)(j) +
                                           hxhy * mesh.ztz2Staggr(zCoreRange)(k)))/(inputData.Re * hxhyhz));

        V->Vx.F.F = jacGuess.Vx.F.F;

        imposeUBCs();

        lapField.Vx.F.F(V->Vx.F.fBulk) = V->Vx.F.F(V->Vx.F.fBulk) - (
            mesh.xix2Colloc(xCoreRange)(i) * (V->Vx.F.F(V->Vx.F.fBRgt) - 2.0 * V->Vx.F.F(V->Vx.F.fBulk) + V->Vx.F.F(V->Vx.F.fBLft)) / (hx * hx) +
            mesh.ety2Staggr(yCoreRange)(j) * (V->Vx.F.F(V->Vx.F.fBBak) - 2.0 * V->Vx.F.F(V->Vx.F.fBulk) + V->Vx.F.F(V->Vx.F.fBFrt)) / (hy * hy) +
            mesh.ztz2Staggr(zCoreRange)(k) * (V->Vx.F.F(V->Vx.F.fBTop) - 2.0 * V->Vx.F.F(V->Vx.F.fBulk) + V->Vx.F.F(V->Vx.F.fBBot)) / (hz * hz)) *
                                       0.5 * inputData.tStp / inputData.Re;

        lapField.Vx.F.F(V->Vx.F.fBulk) = abs(lapField.Vx.F.F(V->Vx.F.fBulk) - H->Vx.F.F(V->Vx.F.fBulk));

        maxValue = lapField.Vx.fieldMax();

        if (maxValue < inputData.tolerance) {
            if (tStepCnt % inputData.ioCnt == 0) {
                if (mesh.rankData.rank == 0) {
                    std::cout << "\033[32mJacobi solver for U converged in " << iterCount << " iterations\033[0m" << std::endl;
                }
            }
            break;
        }

        iterCount += 1;

        if (iterCount > maxCount) {
            if (mesh.rankData.rank == 0) {
                std::cout << "\033[31mERROR:\033[0m Jacobi iterations for solution at coarsest level not converging. Aborting" << std::endl;
                exit(0);
            }
            break;
        }
    }
}

void hydro3::vSolve() {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    int iterCount = 0;

    xCoreRange = blitz::Range(V->Vy.F.fBulk.lbound(0), V->Vy.F.fBulk.ubound(0));
    yCoreRange = blitz::Range(V->Vy.F.fBulk.lbound(1), V->Vy.F.fBulk.ubound(1));
    zCoreRange = blitz::Range(V->Vy.F.fBulk.lbound(2), V->Vy.F.fBulk.ubound(2));

    while (true) {
        jacGuess.Vy.F.F(V->Vy.F.fBulk) = ((hyhz * mesh.xix2Staggr(xCoreRange)(i) * (V->Vy.F.F(V->Vy.F.fBRgt) + V->Vy.F.F(V->Vy.F.fBLft)) +
                                                hzhx * mesh.ety2Colloc(yCoreRange)(j) * (V->Vy.F.F(V->Vy.F.fBBak) + V->Vy.F.F(V->Vy.F.fBFrt)) +
                                                hxhy * mesh.ztz2Staggr(zCoreRange)(k) * (V->Vy.F.F(V->Vy.F.fBTop) + V->Vy.F.F(V->Vy.F.fBBot))) *
                             inputData.tStp / ( hxhyhz * 2.0 * inputData.Re) + H->Vy.F.F(V->Vy.F.fBulk))/
                      (1.0 + inputData.tStp * ((hyhz * mesh.xix2Staggr(xCoreRange)(i) +
                                                hzhx * mesh.ety2Colloc(yCoreRange)(j) +
                                                hxhy * mesh.ztz2Staggr(zCoreRange)(k)))/(inputData.Re * hxhyhz));

        V->Vy.F.F = jacGuess.Vy.F.F;

        imposeVBCs();

        lapField.Vy.F.F(V->Vy.F.fBulk) = V->Vy.F.F(V->Vy.F.fBulk) - (
            mesh.xix2Staggr(xCoreRange)(i) * (V->Vy.F.F(V->Vy.F.fBRgt) - 2.0 * V->Vy.F.F(V->Vy.F.fBulk) + V->Vy.F.F(V->Vy.F.fBLft)) / (hx * hx) +
            mesh.ety2Colloc(yCoreRange)(j) * (V->Vy.F.F(V->Vy.F.fBBak) - 2.0 * V->Vy.F.F(V->Vy.F.fBulk) + V->Vy.F.F(V->Vy.F.fBFrt)) / (hy * hy) +
            mesh.ztz2Staggr(zCoreRange)(k) * (V->Vy.F.F(V->Vy.F.fBTop) - 2.0 * V->Vy.F.F(V->Vy.F.fBulk) + V->Vy.F.F(V->Vy.F.fBBot)) / (hz * hz)) *
                                       0.5 * inputData.tStp / inputData.Re;

        lapField.Vy.F.F(V->Vy.F.fBulk) = abs(lapField.Vy.F.F(V->Vy.F.fBulk) - H->Vy.F.F(V->Vy.F.fBulk));

        maxValue = lapField.Vy.fieldMax();

        if (maxValue < inputData.tolerance) {
            if (tStepCnt % inputData.ioCnt == 0) {
                if (mesh.rankData.rank == 0) {
                    std::cout << "\033[32mJacobi solver for V converged in " << iterCount << " iterations\033[0m" << std::endl;
                }
            }
            break;
        }

        iterCount += 1;

        if (iterCount > maxCount) {
            if (mesh.rankData.rank == 0) {
                std::cout << "\033[31mERROR:\033[0m Jacobi iterations for solution at coarsest level not converging. Aborting" << std::endl;
                exit(0);
            }
            break;
        }
    }
}

void hydro3::wSolve() {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    int iterCount = 0;

    xCoreRange = blitz::Range(V->Vz.F.fBulk.lbound(0), V->Vz.F.fBulk.ubound(0));
    yCoreRange = blitz::Range(V->Vz.F.fBulk.lbound(1), V->Vz.F.fBulk.ubound(1));
    zCoreRange = blitz::Range(V->Vz.F.fBulk.lbound(2), V->Vz.F.fBulk.ubound(2));

    while (true) {
        jacGuess.Vz.F.F(V->Vz.F.fBulk) = ((hyhz * mesh.xix2Staggr(xCoreRange)(i) * (V->Vz.F.F(V->Vz.F.fBRgt) + V->Vz.F.F(V->Vz.F.fBLft)) +
                                           hzhx * mesh.ety2Staggr(yCoreRange)(j) * (V->Vz.F.F(V->Vz.F.fBBak) + V->Vz.F.F(V->Vz.F.fBFrt)) +
                                           hxhy * mesh.ztz2Colloc(zCoreRange)(k) * (V->Vz.F.F(V->Vz.F.fBTop) + V->Vz.F.F(V->Vz.F.fBBot))) *
                        inputData.tStp / ( hxhyhz * 2.0 * inputData.Re) + H->Vz.F.F(V->Vz.F.fBulk))/
                 (1.0 + inputData.tStp * ((hyhz * mesh.xix2Staggr(xCoreRange)(i) +
                                           hzhx * mesh.ety2Staggr(yCoreRange)(j) +
                                           hxhy * mesh.ztz2Colloc(zCoreRange)(k)))/(inputData.Re * hxhyhz));

        V->Vz.F.F = jacGuess.Vz.F.F;

        imposeWBCs();

        lapField.Vz.F.F(V->Vz.F.fBulk) = V->Vz.F.F(V->Vz.F.fBulk) - (
            mesh.xix2Staggr(xCoreRange)(i) * (V->Vz.F.F(V->Vz.F.fBRgt) - 2.0 * V->Vz.F.F(V->Vz.F.fBulk) + V->Vz.F.F(V->Vz.F.fBLft)) / (hx * hx) +
            mesh.ety2Staggr(yCoreRange)(j) * (V->Vz.F.F(V->Vz.F.fBBak) - 2.0 * V->Vz.F.F(V->Vz.F.fBulk) + V->Vz.F.F(V->Vz.F.fBFrt)) / (hy * hy) +
            mesh.ztz2Colloc(zCoreRange)(k) * (V->Vz.F.F(V->Vz.F.fBTop) - 2.0 * V->Vz.F.F(V->Vz.F.fBulk) + V->Vz.F.F(V->Vz.F.fBBot)) / (hz * hz)) *
                                       0.5 * inputData.tStp / inputData.Re;

        lapField.Vz.F.F(V->Vz.F.fBulk) = abs(lapField.Vz.F.F(V->Vz.F.fBulk) - H->Vz.F.F(V->Vz.F.fBulk));

        maxValue = lapField.Vz.fieldMax();

        if (maxValue < inputData.tolerance) {
            if (tStepCnt % inputData.ioCnt == 0) {
                if (mesh.rankData.rank == 0) {
                    std::cout << "\033[32mJacobi solver for W converged in " << iterCount << " iterations\033[0m" << std::endl;
                }
            }
            break;
        }

        iterCount += 1;

        if (iterCount > maxCount) {
            if (mesh.rankData.rank == 0) {
                std::cout << "\033[31mERROR:\033[0m Jacobi iterations for solution at coarsest level not converging. Aborting" << std::endl;
                exit(0);
            }
            break;
        }
    }
}

void hydro3::setCoefficients() {
    hx = mesh.dXi;
    hy = mesh.dEt;
    hz = mesh.dZt;

    hxhy = pow(mesh.dXi, 2.0)*pow(mesh.dEt, 2.0);
    hyhz = pow(mesh.dEt, 2.0)*pow(mesh.dZt, 2.0);
    hzhx = pow(mesh.dZt, 2.0)*pow(mesh.dXi, 2.0);

    hxhyhz = pow(mesh.dXi, 2.0)*pow(mesh.dEt, 2.0)*pow(mesh.dZt, 2.0);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to impose global and sub-domain boundary values on x-velocity component
 *
 *          First, inter-processor data transfer is performed by calling the \ref mpidata#syncData "syncData" function of the
 *          \ref sfield#mpiHandle "mpiHandle" of the <B>Vx</B> component.
 *          Then the values of <B>Vx</B> on the 6 walls are imposed.
 *          The order of imposing boundary conditions is - left, right, front, back, bottom and top boundaries.
 *          The corner values are not being imposed specifically and is thus dependent on the above order.
 ********************************************************************************************************************************************
 */
void hydro3::imposeUBCs() {
    V->Vx.mpiHandle->syncData();

    if (inputData.probType == "LDC") {
        // Vx LIES ON EITHER SIDE OF THE LEFT WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS COLLOCATED ALONG X
        if (mesh.rankData.xRank == 0) {
            V->Vx.F.F(V->Vx.F.fWalls(0)) = -V->Vx.F.F(V->Vx.F.xDeriv->shift(V->Vx.F.fWalls(0), 1));
        }
        // Vx LIES ON EITHER SIDE OF THE RIGHT WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS COLLOCATED ALONG X
        if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
            V->Vx.F.F(V->Vx.F.fWalls(1)) = -V->Vx.F.F(V->Vx.F.xDeriv->shift(V->Vx.F.fWalls(1), -1));
        }

        // Vx LIES ON THE FRONT WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Y
        if (mesh.rankData.yRank == 0) {
            V->Vx.F.F(V->Vx.F.fWalls(2)) = 0.0;
        }
        // Vx LIES ON THE BACK WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Y
        if (mesh.rankData.yRank == mesh.rankData.npY - 1) {
            V->Vx.F.F(V->Vx.F.fWalls(3)) = 0.0;
        }

        // Vx LIES ON THE BOTTOM WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Z
        V->Vx.F.F(V->Vx.F.fWalls(4)) = 0.0;
        // Vx LIES ON THE TOP WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Z
        V->Vx.F.F(V->Vx.F.fWalls(5)) = 1.0;

    } else if (inputData.probType == "TGV") {
        V->Vx.F.F(V->Vx.F.fWalls(4)) = V->Vx.F.F(V->Vx.F.zDeriv->shift(V->Vx.F.fWalls(5), -2));
        V->Vx.F.F(V->Vx.F.fWalls(5)) = V->Vx.F.F(V->Vx.F.zDeriv->shift(V->Vx.F.fWalls(4), 2));
    }
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to impose global and sub-domain boundary values on y-velocity component
 *
 *          First, inter-processor data transfer is performed by calling the \ref mpidata#syncData "syncData" function of the
 *          \ref sfield#mpiHandle "mpiHandle" of the <B>Vy</B> component.
 *          Then the values of <B>Vy</B> on the 6 walls are imposed.
 *          The order of imposing boundary conditions is - left, right, front, back, bottom and top boundaries.
 *          The corner values are not being imposed specifically and is thus dependent on the above order.
 ********************************************************************************************************************************************
 */
void hydro3::imposeVBCs() {
    V->Vy.mpiHandle->syncData();

    if (inputData.probType == "LDC") {
        // Vy LIES ON THE LEFT WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS STAGGERED ALONG X
        if (mesh.rankData.xRank == 0) {
            V->Vy.F.F(V->Vy.F.fWalls(0)) = 0.0;
        }
        // Vy LIES ON THE RIGHT WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS STAGGERED ALONG X
        if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
            V->Vy.F.F(V->Vy.F.fWalls(1)) = 0.0;
        }

        // Vy LIES ON EITHER SIDE OF THE FRONT WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS COLLOCATED ALONG Y
        if (mesh.rankData.yRank == 0) {
            V->Vy.F.F(V->Vy.F.fWalls(2)) = -V->Vy.F.F(V->Vy.F.yDeriv->shift(V->Vy.F.fWalls(2), 1));
        }
        // Vy LIES ON EITHER SIDE OF THE BACK WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS COLLOCATED ALONG Y
        if (mesh.rankData.yRank == mesh.rankData.npY - 1) {
            V->Vy.F.F(V->Vy.F.fWalls(3)) = -V->Vy.F.F(V->Vy.F.yDeriv->shift(V->Vy.F.fWalls(3), -1));
        }

        // Vy LIES ON THE BOTTOM WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS STAGGERED ALONG Z
        V->Vy.F.F(V->Vy.F.fWalls(4)) = 0.0;
        // Vy LIES ON THE TOP WALL AS THE WALL IS ON STAGGERED POINT AND Vy IS STAGGERED ALONG Z
        V->Vy.F.F(V->Vy.F.fWalls(5)) = 0.0;

    } else if (inputData.probType == "TGV") {
        V->Vy.F.F(V->Vy.F.fWalls(4)) = V->Vy.F.F(V->Vy.F.zDeriv->shift(V->Vy.F.fWalls(5), -2));
        V->Vy.F.F(V->Vy.F.fWalls(5)) = V->Vy.F.F(V->Vy.F.zDeriv->shift(V->Vy.F.fWalls(4), 2));
    }
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to impose global and sub-domain boundary values on z-velocity component
 *
 *          First, inter-processor data transfer is performed by calling the \ref mpidata#syncData "syncData" function of the
 *          \ref sfield#mpiHandle "mpiHandle" of the <B>Vz</B> component.
 *          Then the values of <B>Vz</B> on the 6 walls are imposed.
 *          The order of imposing boundary conditions is - left, right, front, back, bottom and top boundaries.
 *          The corner values are not being imposed specifically and is thus dependent on the above order.
 ********************************************************************************************************************************************
 */
void hydro3::imposeWBCs() {
    V->Vz.mpiHandle->syncData();

    if (inputData.probType == "LDC") {
        // Vz LIES ON THE LEFT WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG X
        if (mesh.rankData.xRank == 0) {
            V->Vz.F.F(V->Vz.F.fWalls(0)) = 0.0;
        }
        // Vz LIES ON THE RIGHT WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG X
        if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
            V->Vz.F.F(V->Vz.F.fWalls(1)) = 0.0;
        }

        // Vz LIES ON THE FRONT WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG Y
        if (mesh.rankData.yRank == 0) {
            V->Vz.F.F(V->Vz.F.fWalls(2)) = 0.0;
        }
        // Vz LIES ON THE BACK WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG Y
        if (mesh.rankData.yRank == mesh.rankData.npY - 1) {
            V->Vz.F.F(V->Vz.F.fWalls(3)) = 0.0;
        }

        // Vz LIES ON EITHER SIDE OF THE BOTTOM WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS COLLOCATED ALONG Z
        V->Vz.F.F(V->Vz.F.fWalls(4)) = -V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(4), 1));
        // Vz LIES ON EITHER SIDE OF THE TOP WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS COLLOCATED ALONG Z
        V->Vz.F.F(V->Vz.F.fWalls(5)) = -V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(5), -1));

    } else if (inputData.probType == "TGV") {
        V->Vz.F.F(V->Vz.F.fWalls(4)) = V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(5), -1));
        V->Vz.F.F(V->Vz.F.fWalls(5)) = V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(4), 1));
    }
}

hydro3::~hydro3() {
    delete P;
    delete V;

    delete H;

    delete dataWriter;
}
