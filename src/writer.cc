#include "writer.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the reader class
 *
 *          The constructor initializes two modes of parallel file writing through calls to their
 *          respective initialization functions.
 *
 * \param   mesh is a const reference to the global data contained in the grid class
 * \param   iField is a const reference to the scalar field whose data is to be written
 ********************************************************************************************************************************************
 */
writer::writer(const grid &mesh, const vfield &velocity, const sfield &pressure): mesh(mesh), velocity(velocity), pressure(pressure) {
    /** Initialize the common global and local limits for both ASCII and HDF5 file writing */
    initLimits();

    /** Initialize the variables necessary for ASCII file writing with base MPI-IO */
    initMPIIO();

#ifdef PLANAR
    blitz::TinyVector<int, 2> fieldSize;

    fieldSize = mesh.staggrCoreSize(0), mesh.staggrCoreSize(2);
    fieldData.resize(fieldSize);
#else
    fieldData.resize(mesh.staggrCoreSize);
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to initialize the global and local limits for setting file views
 *
 *          All the necessary limits of the local arrays with respect to the global array for setting the
 *          dataspace of views HDF5 as well as for MPI-IO appropriately are set here.
 ********************************************************************************************************************************************
 */
void writer::initLimits() {
    blitz::TinyVector<int, 3> stagGlo;
    blitz::TinyVector<int, 3> stagLoc;

    // GLOBAL AND LOCAL SIZES OF THE STAGGERED GRID - USED TO WRITE STAGGERED DATA
    stagLoc = mesh.staggrCoreSize - 1;
    stagGlo = mesh.globalSize;

    // WARNING: HDF5 WRITER APPLIES THE FOLLOWING CONDITIONS FOR RANK 0, NOT THE LAST RANK. CHECK
    if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
        stagLoc(0) += 1;
    }

    if (mesh.rankData.yRank == mesh.rankData.npY - 1) {
        stagLoc(1) += 1;
    }
    stagLoc(2) += 1;

    // UNDO THE REDUCTION BY 1 ALONG Y-DIRECTION FOR PLANAR GRIDS TO AVOID 0 SIZE ALONG Y-DIRECTION
#ifdef PLANAR
    stagLoc(1) = 1;
#endif

    x.reference(mesh.xStaggr);
    y.reference(mesh.yStaggr);
    z.reference(mesh.zStaggr);

    gloSize = stagGlo;
    locSize = stagLoc;
    sdStart = mesh.subarrayStarts;
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to initialize the sub-array data-types used in MPI-IO
 *
 *          The function initializes the integer constants \ref tabWidth, \ref fltWidth and \ref dataPrec for setting the
 *          string limits while performing MPI-IO.
 *          Depending on the number of variables to be written, the variable \ref dataCount is adjusted and finally, using all
 *          these integer values, the length of a single solution file data-line is determined.
 *
 *          Parallel file writing in MPI-IO is accomplished by assigning to each process a view of the section of the file to
 *          be written into.
 *          This is acheived by creating MPI Subarray datatypes for the local sub-domains of the computational
 *          domain within the global domain.
 *          The output file is written in ACSII format for quick post-processing, for which the floating point data is first
 *          converted into string data through an MPI_contiguous data-type of MPI_CHAR.
 *          This derived data-type is subsequently used to create the sub-array data-types.
 ********************************************************************************************************************************************
 */
void writer::initMPIIO() {
    int lenNum;
    int dataCount;

    tabWidth = 2;
    fltWidth = 26;
    dataPrec = 16;

    dataCount = 4;
    lineWidth = dataCount*fltWidth + (dataCount - 1)*tabWidth + 1;

    lenNum = locSize(0)*locSize(1)*locSize(2)*lineWidth;

    dataField = new char[lenNum];

    // DEFINE THE CONTIGUOUS DATA TYPE FOR STORING ONE LINE OF SOLUTION DATA IN A STRING
    MPI_Type_contiguous(lineWidth, MPI_CHAR, &STRING_DATA);
    MPI_Type_commit(&STRING_DATA);

    MPI_Type_create_subarray(3, gloSize.data(), locSize.data(), sdStart.data(), MPI_ORDER_C, STRING_DATA, &DATA_ARRAY);
    MPI_Type_commit(&DATA_ARRAY);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to write staggered mesh data into an output file
 *
 *          MPI-IO is implemented in this function by first writing the numerical data into character array with each
 *          entry containing a single line of the output data file.
 *          The character matrix is then treated as a sub-array of the full data output of the entire computational domain.
 *          First the root rank opens the file and writes the Tecplot data header on the solution file and closes it.
 *          Subsequently all the processes open the file and append to it with each one's data.
 *          The headers are prevented from being overwritten by specifying an offset for the processes while writing the data.
 *
 * \param   time is a double precision value containing the time to be written as SOLUTIONTIME in the output file
 ********************************************************************************************************************************************
 */
void writer::writeASCII(double time) {
    int i, j, k;
    int meshSize;
    int sPtrCount;

    char* dataRep;
    char* fileName;

    std::ostringstream dataLine;
    std::ostringstream constFile;

    MPI_Status writeStat;
    MPI_File filePtr;

    copyData(pressure);

    fileName = new char[100];
    constFile.str(std::string());
    constFile << "output/Soln_" << std::fixed << std::setfill('0') << std::setw(9) << std::setprecision(4) << time << ".dat";
    strcpy(fileName, constFile.str().c_str());

    dataRep = new char[6];
    strcpy(dataRep, "native");

    // RANK 0 OPENS FILE AND WRITES THE HEADER FOR THE OUTPUT FILE
    if (mesh.rankData.rank == 0) {
        std::ofstream ofFile(fileName);
        ofFile << "VARIABLES = X, Y, Z, " << pressure.fieldName << "\n"
               << "ZONE T=S\tI= " << gloSize(0) <<
                          "\tJ= " << gloSize(1) <<
                          "\tK= " << gloSize(2) <<
                          "\tF=POINT\n";
        ofFile.close();
    }

    // AFTER RANK 0 HAS WRITTEN THE HEADER, REOPEN AND GET FILE SIZE TO SET OFFSET FOR MPI-IO
    MPI_Barrier(MPI_COMM_WORLD);
    FILE* ofFile = std::fopen(fileName, "r");
    fseek(ofFile, 0L, SEEK_END);
    MPI_Offset dataOffset = std::ftell(ofFile);
    fclose(ofFile);

    // GENERATE THE ASCII DATA FIELD TO BE SUBSEQUENTLY WRITTEN ONTO THE OUTPUT FILE
    sPtrCount = 0;
    for (i=0; i<locSize(0); i++) {
        for (j=0; j<locSize(1); j++) {
            for (k=0; k<locSize(2); k++) {
                dataLine.str(std::string());
                dataLine << std::fixed << std::setw(fltWidth + tabWidth) << std::setprecision(dataPrec) << x(i)
                                       << std::setw(fltWidth + tabWidth) << std::setprecision(dataPrec) << y(j)
                                       << std::setw(fltWidth + tabWidth) << std::setprecision(dataPrec) << z(k)
                                       << std::setw(fltWidth) << std::setprecision(dataPrec) << pressure.F.F(i, j, k) << std::endl;
                strcpy(&dataField[sPtrCount], dataLine.str().c_str());
                sPtrCount += lineWidth;
            }
        }
    }

    // MPI-IO
    meshSize = locSize(0)*locSize(1)*locSize(2);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_File_open(MPI_COMM_WORLD, fileName, MPI_MODE_WRONLY+MPI_MODE_APPEND, MPI_INFO_NULL, &filePtr);
    MPI_File_set_view(filePtr, dataOffset, STRING_DATA, DATA_ARRAY, dataRep, MPI_INFO_NULL);
    MPI_File_write_all(filePtr, dataField, meshSize, STRING_DATA, &writeStat);
    MPI_File_close(&filePtr);

    delete dataRep;
    delete fileName;
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to write output in HDF5 format in parallel
 *
 *          The function creates source and target dataspaces to set appropriate views of the data
 *          It opens a file in the output folder and all the processors write in parallel into the file
 *
 * \param   time is a double precision value containing the time to be used for naming the file
 ********************************************************************************************************************************************
 */
void writer::writeHDF5(double time) {
    hid_t plist_id;

    hid_t fileHandle;

    hid_t dataSet;

    hid_t sourceDSpace;
    hid_t targetDSpace;

#ifdef PLANAR
    hsize_t dimsf[2];           /* dataset dimensions */
    hsize_t offset[2];          /* offset of hyperslab */
#else
    hsize_t dimsf[3];           /* dataset dimensions */
    hsize_t offset[3];          /* offset of hyperslab */
#endif

    herr_t status;

    std::ostringstream constFile;

    char* fileName;

    /*
     * Detailed documentation of steps for HDF5 file writing in parallel
     */

    // Create a property list for collectively opening a file by all processors
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    // Generate the filename corresponding to the solution file
    fileName = new char[100];
    constFile.str(std::string());
    constFile << "output/Soln_" << std::fixed << std::setfill('0') << std::setw(9) << std::setprecision(4) << time << ".h5";
    strcpy(fileName, constFile.str().c_str());

    // First create a file handle with the path to the output file
    fileHandle = H5Fcreate(fileName, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);

    // Close the property list for later reuse
    H5Pclose(plist_id);

    // Create a dataspace representing the full limits of the array - this is the source dataspace
#ifdef PLANAR
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    sourceDSpace = H5Screate_simple(2, dimsf, NULL);
#else
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    dimsf[2] = fieldData.shape()[2];
    sourceDSpace = H5Screate_simple(3, dimsf, NULL);
#endif

    // Modify the view of the *source* dataspace by using a hyperslab - *this view will be used to read from memory*

    // ************* NOTE: Check if the hyperslab view must be changed to take care of the common point at the the MPI decomposed sub-domain boundaries ****************//
#ifdef PLANAR
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    offset[0] = 0;
    offset[1] = 0;
#else
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    dimsf[2] = fieldData.shape()[2];
    offset[0] = 0;
    offset[1] = 0;
    offset[2] = 0;
#endif

    status = H5Sselect_hyperslab(sourceDSpace, H5S_SELECT_SET, offset, NULL, dimsf, NULL);
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in creating hyperslab while writing data. Aborting" << std::endl;
        }
        exit(0);
    }

    // Create a dataspace representing the full limits of the global array - i.e. the dataspace for output file
#ifdef PLANAR
    dimsf[0] = gloSize(0);
    dimsf[1] = gloSize(2);
    targetDSpace = H5Screate_simple(2, dimsf, NULL);
#else
    dimsf[0] = gloSize(0);
    dimsf[1] = gloSize(1);
    dimsf[2] = gloSize(2);
    targetDSpace = H5Screate_simple(3, dimsf, NULL);
#endif

    // Modify the view of the *target* dataspace by using a hyperslab according to its position in the global file dataspace
#ifdef PLANAR
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    offset[0] = sdStart[0];
    offset[1] = sdStart[2];
#else
    dimsf[0] = fieldData.shape()[0];
    dimsf[1] = fieldData.shape()[1];
    dimsf[2] = fieldData.shape()[2];
    offset[0] = sdStart[0];
    offset[1] = sdStart[1];
    offset[2] = sdStart[2];
#endif

    status = H5Sselect_hyperslab(targetDSpace, H5S_SELECT_SET, offset, NULL, dimsf, NULL);
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in creating hyperslab while writing data. Aborting" << std::endl;
        }
        exit(0);
    }

    // Create a property list to use collective data write
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

    //Write pressure data
    copyData(pressure);

    // Create the dataset *for the file*, linking it to the file handle.
    // Correspondingly, it will use the *core* dataspace, as only the core has to be written excluding the pads
    dataSet = H5Dcreate2(fileHandle, pressure.fieldName.c_str(), H5T_NATIVE_DOUBLE, targetDSpace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    // Write the dataset. Most important thing to note is that the 3rd and 4th arguments represent the *source* and *destination* dataspaces.
    // The source here is the sourceDSpace pointing to the memory buffer. Note that its view has been adjusted using hyperslab.
    // The destination is the targetDSpace. Though the targetDSpace is smaller than the sourceDSpace,
    // only the appropriate hyperslab within the sourceDSpace is transferred to the destination.
    status = H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, sourceDSpace, targetDSpace, plist_id, fieldData.dataFirst());
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in writing output to HDF file. Aborting" << std::endl;
        }
        exit(0);
    }

    H5Dclose(dataSet);

    //Write x-velocity data
    copyData(velocity.Vx);

    dataSet = H5Dcreate2(fileHandle, velocity.Vx.fieldName.c_str(), H5T_NATIVE_DOUBLE, targetDSpace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    status = H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, sourceDSpace, targetDSpace, plist_id, fieldData.dataFirst());
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in writing output to HDF file. Aborting" << std::endl;
        }
        exit(0);
    }

    H5Dclose(dataSet);

#ifndef PLANAR
    //Write y-velocity data
    copyData(velocity.Vy);

    dataSet = H5Dcreate2(fileHandle, velocity.Vy.fieldName.c_str(), H5T_NATIVE_DOUBLE, targetDSpace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    status = H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, sourceDSpace, targetDSpace, plist_id, fieldData.dataFirst());
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in writing output to HDF file. Aborting" << std::endl;
        }
        exit(0);
    }

    H5Dclose(dataSet);
#endif

    //Write z-velocity data
    copyData(velocity.Vz);

    dataSet = H5Dcreate2(fileHandle, velocity.Vz.fieldName.c_str(), H5T_NATIVE_DOUBLE, targetDSpace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    status = H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, sourceDSpace, targetDSpace, plist_id, fieldData.dataFirst());
    if (status) {
        if (mesh.rankData.rank == 0) {
            std::cout << "Error in writing output to HDF file. Aborting" << std::endl;
        }
        exit(0);
    }

    H5Dclose(dataSet);

    // CLOSE/RELEASE RESOURCES
    H5Sclose(sourceDSpace);
    H5Sclose(targetDSpace);
    H5Pclose(plist_id);
    H5Fclose(fileHandle);

    delete fileName;
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to interpolate data to staggered grid if not already so
 *
 *          The writer writes all data at the staggered grid locations to avoid confusion while plotting
 *          Consequently, collocated data is interpolated onto staggered points before writing
 *
 ********************************************************************************************************************************************
 */
void writer::copyData(const sfield &outField) {
    // The below method does not recognize arrays with staggering in more than 1 direction
    // Use with care for exotic arrays

#ifdef PLANAR
    if (not outField.xStag) {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                fieldData(i, k) = (outField.F.F(i-1, 0, k) + outField.F.F(i, 0, k))*0.5;
            }
        }
    } else if (not outField.zStag) {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                fieldData(i, k) = (outField.F.F(i, 0, k-1) + outField.F.F(i, 0, k))*0.5;
            }
        }
    } else {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                fieldData(i, k) = outField.F.F(i, 0, k);
            }
        }
    }
#else
    if (not outField.xStag) {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int j=0; j < mesh.staggrCoreSize(1); j++) {
                for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                    fieldData(i, j, k) = (outField.F.F(i-1, j, k) + outField.F.F(i, j, k))*0.5;
                }
            }
        }
    } else if (not outField.yStag) {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int j=0; j < mesh.staggrCoreSize(1); j++) {
                for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                    fieldData(i, j, k) = (outField.F.F(i, j-1, k) + outField.F.F(i, j, k))*0.5;
                }
            }
        }
    } else if (not outField.zStag) {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int j=0; j < mesh.staggrCoreSize(1); j++) {
                for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                    fieldData(i, j, k) = (outField.F.F(i, j, k-1) + outField.F.F(i, j, k))*0.5;
                }
            }
        }
    } else {
        for (int i=0; i < mesh.staggrCoreSize(0); i++) {
            for (int j=0; j < mesh.staggrCoreSize(1); j++) {
                for (int k=0; k < mesh.staggrCoreSize(2); k++) {
                    fieldData(i, j, k) = outField.F.F(i, j, k);
                }
            }
        }
    }
#endif
}

writer::~writer() {
    delete dataField;
}
