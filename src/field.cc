#include "field.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the field class
 *
 *          The field class decides the limits necessary for a 3D array to store the data as per the specified grid staggering details.
 *          It initializes and stores necessary RectDomain objects for getting the core slice and various offset slices for performing
 *          finite difference operations.
 *          Correspondingly, three instances of the \ref differ class are also initialized to compute derivatives along the three directions.
 *          The upper and lower bounds necessary for the array are also calculated depending on the directions along which the mesh is
 *          staggered and those along which it is collocated.
 *          Finally, a blitz array to store the data of the field is resized according to the limits and initialized to 0.
 *
 * \param   gridData is a const reference to the global data contained in the grid class
 * \param   xStag is a const boolean value that is <B>true</B> when the grid is staggered along the x-direction and <B>false</B> when it is not
 * \param   yStag is a const boolean value that is <B>true</B> when the grid is staggered along the y-direction and <B>false</B> when it is not
 * \param   zStag is a const boolean value that is <B>true</B> when the grid is staggered along the z-direction and <B>false</B> when it is not
 ********************************************************************************************************************************************
 */
field::field(const grid &gridData, const bool xStag, const bool yStag, const bool zStag): gridData(gridData) {
    xDeriv = new differ(0, gridData.dXi);
    yDeriv = new differ(1, gridData.dEt);
    zDeriv = new differ(2, gridData.dZt);

    fSize = gridData.collocFullSize;
    flBound = gridData.collocFullDomain.lbound();

    if (xStag) {
        fSize(0) = gridData.staggrFullSize(0);
        flBound(0) = gridData.staggrFullDomain.lbound()(0);
    }

    if (yStag) {
        fSize(1) = gridData.staggrFullSize(1);
        flBound(1) = gridData.staggrFullDomain.lbound()(1);
    }

    if (zStag) {
        fSize(2) = gridData.staggrFullSize(2);
        flBound(2) = gridData.staggrFullDomain.lbound()(2);
    }

    F.resize(fSize);
    F.reindexSelf(flBound);

    setCoreSlice(xStag, yStag, zStag);
    setBulkSlice(xStag, yStag, zStag);

    setWallSlices();

    F = 0.0;
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to create the core slice and its offset views
 *
 *          The core and full slices of the field differentiates the domain into the computational sub-domain and
 *          the ghost point/pad point regions which play only an auxilliary role in computing the derivatives.
 *          The core slice defined here also includes the walls of the domain.
 *          The core slice is also offset in all the directions for ease in computation of numerical derivatives.
 *
 * \param   xStag is a const boolean value that is <B>true</B> when the grid is staggered along the x-direction and <B>false</B> when it is not
 * \param   yStag is a const boolean value that is <B>true</B> when the grid is staggered along the y-direction and <B>false</B> when it is not
 * \param   zStag is a const boolean value that is <B>true</B> when the grid is staggered along the z-direction and <B>false</B> when it is not
 ********************************************************************************************************************************************
 */
void field::setCoreSlice(const bool xStag, const bool yStag, const bool zStag) {
    cuBound = gridData.collocCoreDomain.ubound();

    if (xStag) {
        cuBound(0) = gridData.staggrCoreDomain.ubound()(0);
    }

    if (yStag) {
        cuBound(1) = gridData.staggrCoreDomain.ubound()(1);
    }

    if (zStag) {
        cuBound(2) = gridData.staggrCoreDomain.ubound()(2);
    }

    fCore = blitz::RectDomain<3>(blitz::TinyVector<int, 3>(0, 0, 0), cuBound);

    fCLft = xDeriv->shift(fCore, -1);
    fCRgt = xDeriv->shift(fCore,  1);

    fCFrt = yDeriv->shift(fCore, -1);
    fCBak = yDeriv->shift(fCore,  1);

    fCBot = zDeriv->shift(fCore, -1);
    fCTop = zDeriv->shift(fCore,  1);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to create the bulk slice and its offset views
 *
 *          The bulk and wall slices of the field differentiates the domain into the bulk of the fluid and
 *          the walls of the domain.
 *          The bulk slice is also offset in all the directions for ease in computation of numerical derivatives.
 *
 * \param   xStag is a const boolean value that is <B>true</B> when the grid is staggered along the x-direction and <B>false</B> when it is not
 * \param   yStag is a const boolean value that is <B>true</B> when the grid is staggered along the y-direction and <B>false</B> when it is not
 * \param   zStag is a const boolean value that is <B>true</B> when the grid is staggered along the z-direction and <B>false</B> when it is not
 ********************************************************************************************************************************************
 */
void field::setBulkSlice(const bool xStag, const bool yStag, const bool zStag) {
    blitz::TinyVector<int, 3> blBound;
    blitz::TinyVector<int, 3> buBound;

    blBound = gridData.collocCoreDomain.lbound();
    buBound = gridData.collocCoreDomain.ubound();

    if (xStag) {
        blBound(0) = gridData.staggrCoreDomain.lbound()(0);
        buBound(0) = gridData.staggrCoreDomain.ubound()(0);
    }

    if (yStag) {
        blBound(1) = gridData.staggrCoreDomain.lbound()(1);
        buBound(1) = gridData.staggrCoreDomain.ubound()(1);
    }

    if (zStag) {
        blBound(2) = gridData.staggrCoreDomain.lbound()(2);
        buBound(2) = gridData.staggrCoreDomain.ubound()(2);
    }

    // Bulk and core slices are differentiated only in the boundary sub-domains,
    // and that differentiation is imposed in the following lines
    // At all interior sub-domains after performing MPI domain decomposition,
    // the bulk and core slices are identical

    // ** WARNING **
    // The bulk is modified only for non-periodic problems
    // Hence for TGV cases, the bulk is not being changed.
    // This is not a proper method!! A better method must be worked out
    if (gridData.inputData.probType != "TGV") {
        if (xStag and gridData.subarrayStarts(0) == 0) blBound(0) += 1;

        if (xStag and gridData.subarrayEnds(0) == gridData.globalSize(0) - 1) buBound(0) -= 1;

        if (yStag and gridData.subarrayStarts(1) == 0) blBound(1) += 1;

        if (yStag and gridData.subarrayEnds(1) == gridData.globalSize(1) - 1) buBound(1) -= 1;

        if (zStag) {
            blBound(2) += 1;
            buBound(2) -= 1;
        }
    }

#ifdef PLANAR
    blBound(1) = 0;
    buBound(1) = 0;
#endif

    fBulk = blitz::RectDomain<3>(blBound, buBound);

    fBLft = xDeriv->shift(fBulk, -1);
    fBRgt = xDeriv->shift(fBulk,  1);

    fBFrt = yDeriv->shift(fBulk, -1);
    fBBak = yDeriv->shift(fBulk,  1);

    fBBot = zDeriv->shift(fBulk, -1);
    fBTop = zDeriv->shift(fBulk,  1);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to create the wall slices for the sub-domains
 *
 *          The wall slices of the sub-domain are for imposing the full-domain boundary conditions and hence are of importance
 *          only for the near-boundary sub-domains in parallel computations.
 *          Moreover, only the collocated grid has points on the boundary.
 *          The staggered grid points lie on either side of the domain boundaries.
 *          As a result the wall slices are defined only for those fields for which at least one of \ref xStag, \ref yStag or
 *          \ref zStag is false
 ********************************************************************************************************************************************
 */
void field::setWallSlices() {
    blitz::Array<blitz::TinyVector<int, 3>, 1> wlBound;
    blitz::Array<blitz::TinyVector<int, 3>, 1> wuBound;

    // 6 slices are stored in fWalls corresponding to the 6 faces of the 3D box
    fWalls.resize(6);

    wlBound.resize(6);
    wuBound.resize(6);

    // Wall slices are the locations where the BC (both Neumann and Dirichlet) is imposed.
    // In the places where these slices are being used, they should be on the LHS of equation.
    for (int i=0; i<6; i++) {
        wlBound(i) = F.lbound();
        wuBound(i) = F.ubound();
    }

    // The bulk slice corresponds to the part of the fluid within which all variables are computed at each time step.
    // Correspondingly, the boundary conditions are imposed on the layer just outside the bulk

    // UPPER BOUNDS OF LEFT WALL
    wlBound(0)(0) = wuBound(0)(0) = fBulk.lbound(0) - 1;

    // LOWER BOUNDS OF RIGHT WALL
    wuBound(1)(0) = wlBound(1)(0) = fBulk.ubound(0) + 1;

    // UPPER BOUNDS OF FRONT WALL
    wlBound(2)(1) = wuBound(2)(1) = fBulk.lbound(1) - 1;

    // LOWER BOUNDS OF BACK WALL
    wuBound(3)(1) = wlBound(3)(1) = fBulk.ubound(1) + 1;

    // UPPER BOUNDS OF BOTTOM WALL
    wlBound(4)(2) = wuBound(4)(2) = fBulk.lbound(2) - 1;

    // LOWER BOUNDS OF TOP WALL
    wuBound(5)(2) = wlBound(5)(2) = fBulk.ubound(2) + 1;

    for (int i=0; i<6; i++) {
        fWalls(i) = blitz::RectDomain<3>(wlBound(i), wuBound(i));
    }
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the first derivative along X direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::x1Deriv(blitz::Array<double, 3> dF) {
    xDeriv->D1D(F, dF, fCore);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the first derivative along Y direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::y1Deriv(blitz::Array<double, 3> dF) {
    yDeriv->D1D(F, dF, fCore);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the first derivative along Z direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::z1Deriv(blitz::Array<double, 3> dF) {
    zDeriv->D1D(F, dF, fCore);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the second derivative along X direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::x2Deriv(blitz::Array<double, 3> dF) {
    xDeriv->D2D(F, dF, fCore);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the second derivative along Y direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::y2Deriv(blitz::Array<double, 3> dF) {
    yDeriv->D2D(F, dF, fCore);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to calculate the second derivative along Z direction
 *
 *          The first derivative of the field values are calculated using functions from the \ref differ class.
 *          The corresponding \ref differ object has already been initialized in the constructor.
 *
 * \param   dF is an empty 3D blitz matrix of double precision values into which the calculated values of derivative are written
 ********************************************************************************************************************************************
 */
void field::z2Deriv(blitz::Array<double, 3> dF) {
    zDeriv->D2D(F, dF, fCore);
}

field::~field() {
    delete xDeriv;
    delete yDeriv;
    delete zDeriv;
}
