#include "sfield.h"
#include "vfield.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the sfield class
 *
 *          The instance of the field class to store the data of the scalar field is initialized, and the necessary grid
 *          transformation derivatives along each direction are chosen according to the grid staggering.
 *          The arrays to store the output from various operators like derivatives, convective derivatives, etc. are also
 *          allocated.
 *          Finally, an instance of the <B>mpidata</B> class is initialized to store the sub-arrays to be send/received
 *          across the processors during MPI communication.
 *
 * \param   gridData is a const reference to the global data contained in the grid class
 * \param   fieldName is a string value set by the user to name and identify the scalar field
 * \param   xStag is a boolean value that is <B>true</B> when the grid is staggered along the x-direction and <B>false</B> when it is not
 * \param   yStag is a boolean value that is <B>true</B> when the grid is staggered along the y-direction and <B>false</B> when it is not
 * \param   zStag is a boolean value that is <B>true</B> when the grid is staggered along the z-direction and <B>false</B> when it is not
 ********************************************************************************************************************************************
 */
sfield::sfield(const grid &gridData, std::string fieldName, bool xStag, bool yStag, bool zStag):
               gridData(gridData),
               xStag(xStag),
               yStag(yStag),
               zStag(zStag),
               F(gridData, xStag, yStag, zStag)
{
    this->fieldName = fieldName;

    if (xStag) {
        x_Metric.reference(gridData.xi_xStaggr);
        xxMetric.reference(gridData.xixxStaggr);
        x2Metric.reference(gridData.xix2Staggr);
    } else {
        x_Metric.reference(gridData.xi_xColloc);
        xxMetric.reference(gridData.xixxColloc);
        x2Metric.reference(gridData.xix2Colloc);
    }

    if (yStag) {
        y_Metric.reference(gridData.et_yStaggr);
        yyMetric.reference(gridData.etyyStaggr);
        y2Metric.reference(gridData.ety2Staggr);
    } else {
        y_Metric.reference(gridData.et_yColloc);
        yyMetric.reference(gridData.etyyColloc);
        y2Metric.reference(gridData.ety2Colloc);
    }

    if (zStag) {
        z_Metric.reference(gridData.zt_zStaggr);
        zzMetric.reference(gridData.ztzzStaggr);
        z2Metric.reference(gridData.ztz2Staggr);
    } else {
        z_Metric.reference(gridData.zt_zColloc);
        zzMetric.reference(gridData.ztzzColloc);
        z2Metric.reference(gridData.ztz2Colloc);
    }

    setInterpolationSlices();

    dF_dx.resize(F.fSize);
    dF_dx.reindexSelf(F.flBound);

    dF_dy.resize(F.fSize);
    dF_dy.reindexSelf(F.flBound);

    dF_dz.resize(F.fSize);
    dF_dz.reindexSelf(F.flBound);

    d2F_dx2.resize(F.fSize);
    d2F_dx2.reindexSelf(F.flBound);

    d2F_dy2.resize(F.fSize);
    d2F_dy2.reindexSelf(F.flBound);

    d2F_dz2.resize(F.fSize);
    d2F_dz2.reindexSelf(F.flBound);


    interVx.resize(F.fSize);
    interVx.reindexSelf(F.flBound);

    interVy.resize(F.fSize);
    interVy.reindexSelf(F.flBound);

    interVz.resize(F.fSize);
    interVz.reindexSelf(F.flBound);


    mpiHandle = new mpidata(F.F, gridData.rankData);
    mpiHandle->createSubarrays(F.fSize, F.cuBound + 1, gridData.padWidths, xStag, yStag);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to create the slices for interpolation while computing convective derivative
 *
 *          This function must be called before using the values contained in the arrays d2F_dx2, d2F_dy2 and d2F_dz2.
 ********************************************************************************************************************************************
 */
void sfield::setInterpolationSlices() {
    // INTERPOLATION SLICES FOR INTERPOLATING VALUES OF Vx FROM THE vfield
    if (not xStag) {
        VxIntSlices.resize(1);

        VxIntSlices(0) = F.fCore;
    } else {
        if (yStag) {
            if (zStag) {
                // Cell centered configuration
            } else {
                /**Z-Face centered configuration - Vz
                 * Interpolation of data - Vx ---> Vz
                 * Interpolation types:
                 *      - X direction => collocated to staggered
                 *      - Y direction => no change
                 *      - Z direction => staggered to collocated
                 **/
                VxIntSlices.resize(4);

                VxIntSlices(0) = F.fCore;
                VxIntSlices(1) = F.xDeriv->shift(VxIntSlices(0), -1);
                VxIntSlices(2) = F.zDeriv->shift(VxIntSlices(0), 1);
                VxIntSlices(3) = F.xDeriv->shift(VxIntSlices(2), -1);
            }
        } else {
            if (zStag) {
                /**Y-Face centered configuration - Vy
                 * Interpolation of data - Vx ---> Vy
                 * Interpolation types:
                 *      - X direction => collocated to staggered
                 *      - Y direction => staggered to collocated
                 *      - Z direction => no change
                 **/
                VxIntSlices.resize(4);

                VxIntSlices(0) = F.fCore;
                VxIntSlices(1) = F.xDeriv->shift(VxIntSlices(0), -1);
                VxIntSlices(2) = F.yDeriv->shift(VxIntSlices(0), 1);
                VxIntSlices(3) = F.xDeriv->shift(VxIntSlices(2), -1);
            }
        }
    }

    // INTERPOLATION SLICES FOR INTERPOLATING VALUES OF Vy FROM THE vfield
    if (not yStag) {
        VyIntSlices.resize(1);

        VyIntSlices(0) = F.fCore;
    } else {
        if (xStag) {
            if (zStag) {
                // Cell centered configuration
            } else {
                /**Z-Face centered configuration - Vz
                 * Interpolation of data - Vy ---> Vz
                 * Interpolation types:
                 *      - X direction => no change
                 *      - Y direction => collocated to staggered
                 *      - Z direction => staggered to collocated
                 **/
                VyIntSlices.resize(4);

                VyIntSlices(0) = F.fCore;
                VyIntSlices(1) = F.yDeriv->shift(VyIntSlices(0), -1);
                VyIntSlices(2) = F.zDeriv->shift(VyIntSlices(0), 1);
                VyIntSlices(3) = F.yDeriv->shift(VyIntSlices(2), -1);
            }
        } else {
            if (zStag) {
                /**X-Face centered configuration - Vx
                 * Interpolation of data - Vy ---> Vx
                 * Interpolation types:
                 *      - X direction => staggered to collocated
                 *      - Y direction => collocated to staggered
                 *      - Z direction => no change
                 **/
                VyIntSlices.resize(4);

                VyIntSlices(0) = F.fCore;
                VyIntSlices(1) = F.yDeriv->shift(VyIntSlices(0), -1);
                VyIntSlices(2) = F.xDeriv->shift(VyIntSlices(0), 1);
                VyIntSlices(3) = F.yDeriv->shift(VyIntSlices(2), -1);
            }
        }
    }

    // INTERPOLATION SLICES FOR INTERPOLATING VALUES OF Vz FROM THE vfield
    if (not zStag) {
        VzIntSlices.resize(1);

        VzIntSlices(0) = F.fCore;
    } else {
        if (xStag) {
            if (yStag) {
                // Cell centered configuration
            } else {
                /**Y-Face centered configuration - Vy
                 * Interpolation of data - Vz ---> Vy
                 * Interpolation types:
                 *      - X direction => no change
                 *      - Y direction => staggered to collocated
                 *      - Z direction => collocated to staggered
                 **/
                VzIntSlices.resize(4);

                VzIntSlices(0) = F.fCore;
                VzIntSlices(1) = F.zDeriv->shift(VzIntSlices(0), -1);
                VzIntSlices(2) = F.yDeriv->shift(VzIntSlices(0), 1);
                VzIntSlices(3) = F.zDeriv->shift(VzIntSlices(2), -1);
            }
        } else {
            if (yStag) {
                /**X-Face centered configuration - Vx
                 * Interpolation of data - Vz ---> Vx
                 * Interpolation types:
                 *      - X direction => staggered to collocated
                 *      - Y direction => no change
                 *      - Z direction => collocated to staggered
                 **/
                VzIntSlices.resize(4);

                VzIntSlices(0) = F.fCore;
                VzIntSlices(1) = F.zDeriv->shift(VzIntSlices(0), -1);
                VzIntSlices(2) = F.xDeriv->shift(VzIntSlices(0), 1);
                VzIntSlices(3) = F.zDeriv->shift(VzIntSlices(2), -1);
            }
        }
    }

    // RESET INTERPOLATION SLICES FOR PLANAR GRID
#ifdef PLANAR
    if (fieldName == "Vy") {
        VxIntSlices.resize(1);
        VyIntSlices.resize(1);
        VzIntSlices.resize(1);

        VxIntSlices(0) = F.fCore;
        VyIntSlices(0) = F.fCore;
        VzIntSlices(0) = F.fCore;
    } else if (fieldName == "Vx") {
        VyIntSlices.resize(1);

        VyIntSlices(0) = F.fCore;
    } else if (fieldName == "Vz") {
        VyIntSlices.resize(1);

        VyIntSlices(0) = F.fCore;
    }
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the first derivatives of the scalar field along all the three directions
 *
 *          This function must be called before using the values contained in the arrays dF_dx, dF_dy and dF_dz.
 *          The values of these arrays are updated by this function in each call.
 ********************************************************************************************************************************************
 */
void sfield::calcDerivatives1() {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    F.x1Deriv(dF_dx);
    dF_dx = x_Metric(i)*dF_dx(i,j,k);

#ifndef PLANAR
    F.y1Deriv(dF_dy);
    dF_dy = y_Metric(j)*dF_dy(i,j,k);
#endif

    F.z1Deriv(dF_dz);
    dF_dz = z_Metric(k)*dF_dz(i,j,k);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the second derivatives of the scalar field <B>specifically for implicit CN in hydro3</B>
 *
 *          This function must be called before using the values contained in the arrays d2F_dx2, d2F_dy2 and d2F_dz2.
 *          The values of these arrays are updated by this function in each call.
 *          <B>This function changes the values contained in the arrays dF_dx, dF_dy and dF_dz</B>.
 *          As a result, these arrays must be appropriately updated with call to calcDerivatives1 before using them again.
 ********************************************************************************************************************************************
 */
void sfield::calcDerivatives2() {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    F.x1Deriv(dF_dx);
    F.x2Deriv(d2F_dx2);
    d2F_dx2 = xxMetric(i)*dF_dx(i,j,k)/gridData.inputData.Re + x2Metric(i)*d2F_dx2(i,j,k)*0.5/gridData.inputData.Re;

#ifndef PLANAR
    F.y1Deriv(dF_dy);
    F.y2Deriv(d2F_dy2);
    d2F_dy2 = yyMetric(j)*dF_dy(i,j,k)/gridData.inputData.Re + y2Metric(j)*d2F_dy2(i,j,k)*0.5/gridData.inputData.Re;
#endif

    F.z1Deriv(dF_dz);
    F.z2Deriv(d2F_dz2);
    d2F_dz2 = zzMetric(k)*dF_dz(i,j,k)/gridData.inputData.Re + z2Metric(k)*d2F_dz2(i,j,k)*0.5/gridData.inputData.Re;
}

/**
 ********************************************************************************************************************************************
 * \brief   Operator to compute the gradient of the scalar field
 *
 *          The gradient operator computes the gradient of the cell centered scalar field, and stores it into a face-centered staggered
 *          vector field as defined by the tensor operation:
 *          \f$ \nabla f = \frac{\partial f}{\partial x}i + \frac{\partial f}{\partial y}j + \frac{\partial f}{\partial z}k \f$.
 *
 * \param   gradF is a pointer to a vector field (vfield) into which the computed gradient must be written.
 ********************************************************************************************************************************************
 */
void sfield::gradient(vfield &gradF) {
    blitz::firstIndex i;
    blitz::secondIndex j;
    blitz::thirdIndex k;

    blitz::Range xColl, yColl, zColl;

    xColl = blitz::Range(gridData.collocCoreDomain.lbound(0), gridData.collocCoreDomain.ubound(0));
    yColl = blitz::Range(gridData.collocCoreDomain.lbound(1), gridData.collocCoreDomain.ubound(1));
    zColl = blitz::Range(gridData.collocCoreDomain.lbound(2), gridData.collocCoreDomain.ubound(2));

#ifdef PLANAR
    if (xStag and yStag) {
        gradF.Vx.F.F(gradF.Vx.F.fCore) = gridData.xi_xColloc(xColl)(i)*(F.F(gradF.Vx.F.fCRgt) - F.F(gradF.Vx.F.fCore))/gridData.dXi;
        gradF.Vz.F.F(gradF.Vz.F.fCore) = gridData.zt_zColloc(zColl)(k)*(F.F(gradF.Vz.F.fCTop) - F.F(gradF.Vz.F.fCore))/gridData.dZt;
    } else {
        std::cout << "Attempting to calculate gradient of a non-cell-centered scalar field is currently not supported" << std::endl;
        exit(0);
    }
#else
    if (xStag and yStag and zStag) {
        gradF.Vx.F.F(gradF.Vx.F.fCore) = gridData.xi_xColloc(xColl)(i)*(F.F(gradF.Vx.F.fCRgt) - F.F(gradF.Vx.F.fCore))/gridData.dXi;
        gradF.Vy.F.F(gradF.Vy.F.fCore) = gridData.et_yColloc(yColl)(j)*(F.F(gradF.Vy.F.fCBak) - F.F(gradF.Vy.F.fCore))/gridData.dEt;
        gradF.Vz.F.F(gradF.Vz.F.fCore) = gridData.zt_zColloc(zColl)(k)*(F.F(gradF.Vz.F.fCTop) - F.F(gradF.Vz.F.fCore))/gridData.dZt;
    } else {
        std::cout << "Attempting to calculate gradient of a non-cell-centered scalar field is currently not supported" << std::endl;
        exit(0);
    }
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to compute the convective derivative of the scalar field
 *
 *          The function computes for the operator \f$ (\mathbf{u}.\nabla)f \f$ at the grid nodes of the scalar field f.
 *          To do so, the function needs the vector field (vfield) of velocity. It is assumed that the velocity is always
 *          specified at face-centers, and is interpolated accordingly to the scalar field grid points.
 *
 * \param   V is a const reference to a vector field (vfield) that specifies the convection velocity at each point
 ********************************************************************************************************************************************
 */
void sfield::convectiveDerivative(const vfield &V, sfield &H) {
    calcDerivatives1();

    interVx = 0.0;
    interVy = 0.0;
    interVz = 0.0;

    for (unsigned int i=0; i < VxIntSlices.size(); i++) {
        interVx(F.fCore) += V.Vx.F.F(VxIntSlices(i));
    }
#ifndef PLANAR
    for (unsigned int i=0; i < VyIntSlices.size(); i++) {
        interVy(F.fCore) += V.Vy.F.F(VyIntSlices(i));
    }
#endif
    for (unsigned int i=0; i < VzIntSlices.size(); i++) {
        interVz(F.fCore) += V.Vz.F.F(VzIntSlices(i));
    }

#ifdef PLANAR
    H.F.F(F.fCore) = interVx(F.fCore)*dF_dx(F.fCore)/VxIntSlices.size() +
                     interVz(F.fCore)*dF_dz(F.fCore)/VzIntSlices.size();
#else
    H.F.F(F.fCore) = interVx(F.fCore)*dF_dx(F.fCore)/VxIntSlices.size() +
                     interVy(F.fCore)*dF_dy(F.fCore)/VyIntSlices.size() +
                     interVz(F.fCore)*dF_dz(F.fCore)/VzIntSlices.size();
#endif
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to extract the maximum value from the scalar field
 *
 *          The function uses the in-built blitz function to obtain the maximum value in an array.
 *          While performing parallel computation, the function performs an <B>MPI_Allreduce()</B> to get
 *          the global maximum from the entire computational domain.
 *
 * \return  The double precision value of the maximum is returned (it is implicitly assumed that only double precision values are used)
 ********************************************************************************************************************************************
 */
double sfield::fieldMax() {
    double localMax, globalMax;

    localMax = blitz::max(F.F);

    /***************************************************************************************************************
     * DID YOU KNOW?                                                                                               *
     * In the line above, most compilers will not complain even if you omitted the namespace specification blitz:: *
     * This behaviour wasted an hour of my development time (including the effort of making this nice box).        *
     * Check Ref. [4] in README for explanation.                                                                   *
     ***************************************************************************************************************/

    MPI_Allreduce(&localMax, &globalMax, 1, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD);

    return globalMax;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to add a given scalar field
 *
 *          The unary operator += adds a given scalar field to the entire field stored as sfield and returns
 *          a pointer to itself.
 *
 * \param   a is a reference to another sfield to be added to the member field
 *
 * \return  A pointer to itself is returned by the scalar field class to which the operator belongs
 ********************************************************************************************************************************************
 */
sfield& sfield::operator += (sfield &a) {
    F.F += a.F.F;

    return *this;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to subtract a given scalar field
 *
 *          The unary operator -= subtracts a given scalar field from the entire field stored as sfield and returns
 *          a pointer to itself.
 *
 * \param   a is a reference to another sfield to be deducted from the member field
 *
 * \return  A pointer to itself is returned by the scalar field class to which the operator belongs
 ********************************************************************************************************************************************
 */
sfield& sfield::operator -= (sfield &a) {
    F.F -= a.F.F;

    return *this;
}

/**
 ********************************************************************************************************************************************
 * \brief   Overloaded operator to multiply a scalar value to the scalar field
 *
 *          The unary operator *= multiplies a double precision value to the entire field stored as sfield and returns
 *          a pointer to itself.
 *
 * \param   a is a double precision number to be multiplied to the scalar field
 *
 * \return  A pointer to itself is returned by the scalar field class to which the operator belongs
 ********************************************************************************************************************************************
 */
sfield& sfield::operator *= (double a) {
    F.F *= a;

    return *this;
}
