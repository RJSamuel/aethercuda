#include <iostream>
#include "reader.h"
#include "mpi.h"

reader::reader() {
    readYAML();
    checkData();
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to open the yaml file and read the parameters
 *
 *          The function opens the parameters.yaml file and reads the simulation parameters into its member variables that are publicly
 *          accessible.
 ********************************************************************************************************************************************
 */
void reader::readYAML() {
    std::ifstream inFile;

    inFile.open("parameters.yaml", std::ifstream::in);

    YAML::Node yamlNode;
    YAML::Parser parser(inFile);

    parser.GetNextDocument(yamlNode);
    yamlNode["Problem Type"] >> probType;
    yamlNode["Reynolds Number"] >> Re;

    parser.GetNextDocument(yamlNode);
    yamlNode["Mesh Type"] >> meshType;
    yamlNode["X Beta"] >> betaX;
    yamlNode["Y Beta"] >> betaY;
    yamlNode["Z Beta"] >> betaZ;
    yamlNode["X Length"] >> Lx;
    yamlNode["Y Length"] >> Ly;
    yamlNode["Z Length"] >> Lz;
    yamlNode["X Index"] >> xInd;
    yamlNode["Y Index"] >> yInd;
    yamlNode["Z Index"] >> zInd;

    parser.GetNextDocument(yamlNode);
    yamlNode["X Number of Procs"] >> npX;
    yamlNode["Y Number of Procs"] >> npY;

    parser.GetNextDocument(yamlNode);
    yamlNode["Differentiation Scheme"] >> dScheme;
    yamlNode["Integration Scheme"] >> iScheme;
    yamlNode["Time-Step"] >> tStp;
    yamlNode["Final Time"] >> tMax;
    yamlNode["I/O Count"] >> ioCnt;
    yamlNode["File Write Interval"] >> fwInt;

    parser.GetNextDocument(yamlNode);
    yamlNode["Jacobi Tolerance"] >> tolerance;
    yamlNode["V-Cycle Depth"] >> vcDepth;
    yamlNode["V-Cycle Count"] >> vcCount;
    yamlNode["Pre-Smoothing Count"] >> preSmooth;
    yamlNode["Post-Smoothing Count"] >> postSmooth;
    yamlNode["Inter-Smoothing Count"] >> interSmooth;

    inFile.close();
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to perform a check on the consistency of user-set parameters
 *
 *          In order to catch potential errors early on, a few basic checks are performed here to validate the paramters set
 *          by the user.
 *          Additional checks to be performed on the paramters can be added to this function if necessary.
 ********************************************************************************************************************************************
 */
void reader::checkData() {
    int gridSize, localSize, coarsestSize;

    // CHECK IF THE yInd VARIABLE IS SET CORRECTLY FOR A 2D/3D SIMULATION
#ifdef PLANAR
    if (yInd != 0) {
        std::cout << "WARNING: Y Index parameter of YAML file is non-zero although solver has been compiled with PLANAR flag. Setting Y Index to 0" << std::endl;
        yInd = 0;
    }
#else
    if (yInd == 0) {
        std::cout << "ERROR: Y Index parameter of YAML file is 0 for 3D simulation. ABORTING" << std::endl;
        exit(0);
    }
#endif

    // CHECK IF LESS THAN 1 PROCESSOR IS ASKED FOR ALONG X-DIRECTION. IF SO, WARN AND SET IT TO DEFAULT VALUE OF 1
    if (npX < 1) {
        std::cout << "WARNING: Number of processors in X-direction is less than 1. Setting it to 1" << std::endl;
        npX = 1;
    }

    // CHECK IF LESS THAN 1 PROCESSOR IS ASKED FOR ALONG Y-DIRECTION. IF SO, WARN AND SET IT TO DEFAULT VALUE OF 1
    if (npY < 1) {
        std::cout << "WARNING: Number of processors in Y-direction is less than 1. Setting it to 1" << std::endl;
        npY = 1;
    }

    // CHECK IF THE TIME-STEP SET BY USER IS LESS THAN THE MAXIMUM TIME SPECIFIED FOR SIMULATION.
    if (tStp > tMax) {
        std::cout << "ERROR: Time step is larger than the maximum duration assigned for simulation. Aborting" << std::endl;
        exit(0);
    }

    // CHECK IF MORE THAN 1 PROCESSOR IS ASKED FOR ALONG Y-DIRECTION FOR A 2D SIMULATION
    if (yInd == 0 and npY > 1) {
        std::cout << "ERROR: More than 1 processor is specified along Y-direction, but the yInd parameter is set to 0. Aborting" << std::endl;
        exit(0);
    }

    // CHECK IF GRID SIZE SPECIFIED ALONG EACH DIRECTION IS SUFFICIENT ALONG WITH THE DOMAIN DIVIIONS TO REACH THE LOWEST LEVEL OF V-CYCLE DEPTH SPECIFIED
    // ALONG X-DIRECTION
    gridSize = int(pow(2, xInd));
    localSize = gridSize/npX;
    coarsestSize = int(pow(2, vcDepth+1));
    if (localSize < coarsestSize) {
        std::cout << "ERROR: The grid size and domain decomposition along X-direction results in sub-domains too coarse to reach the V-Cycle depth specified. Aborting" << std::endl;
        exit(0);
    }

    // ALONG Y-DIRECTION
#ifndef PLANAR
    gridSize = int(pow(2, yInd));
    localSize = gridSize/npY;
    coarsestSize = int(pow(2, vcDepth+1));
    if (yInd > 0 and localSize < coarsestSize) {
        std::cout << "ERROR: The grid size and domain decomposition along Y-direction results in sub-domains too coarse to reach the V-Cycle depth specified. Aborting" << std::endl;
        exit(0);
    }
#endif

    // ALONG Z-DIRECTION
    gridSize = int(pow(2, zInd));
    coarsestSize = int(pow(2, vcDepth+1));
    if (gridSize < coarsestSize) {
        std::cout << "ERROR: The grid size along Z-direction is too coarse to reach the V-Cycle depth specified. Aborting" << std::endl;
        exit(0);
    }
}
