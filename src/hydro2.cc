#include "hydro.h"

/**
 ********************************************************************************************************************************************
 * \brief   Constructor of the hydro2 class derived from the base hydro class
 *
 *          The constructor passes its arguments to the base hydro class and then initializes all the scalar and
 *          vector fields necessary for solving the NS equations.
 *          The various coefficients for solving the equations are also set by a call to the \ref setCoefficients function.
 *          Based on the problem type specified by the user in the parameters file, and stored by the \ref reader class as
 *          \ref reader#probType "probType", the appropriate initial and boundary conditions are specified.
 *
 * \param   mesh is a const reference to the global data contained in the grid class
 * \param   solParam is a const reference to the user-set parameters contained in the reader class
 ********************************************************************************************************************************************
 */
hydro2::hydro2(const grid &mesh, const reader &solParam, parallel &mpiParam): hydro(mesh, solParam, mpiParam) {
    P = new sfield(mesh, "P", true, true, true);
    V = new vfield(mesh, "V");

    H = new vfield(mesh, "H");

    // CREATE NEW INSTANCE OF THE WRITER OBJECT USING THE GRID
    dataWriter = new writer(mesh, *V, *P);

    // SET VALUES OF COEFFICIENTS USED FOR COMPUTING LAPLACIAN
    setCoefficients();

    // INITIALIZE VARIABLES
    if (inputData.probType == "LDC") {
        // INITIALIZE PRESSURE TO 1.0 THROUGHOUT THE DOMAIN
        P->F.F = 1.0;

        // FOR LDC, SET THE X-VELOCITY OF STAGGERED POINTS ON THE LID TO 1.0
        V->Vx.F.F(blitz::Range::all(), blitz::Range::all(), mesh.staggrCoreDomain.ubound(2)) = 1.0;

        // Disable periodic data transfer by setting neighbouring ranks of boundary sub-domains to NULL
        if (mpiData.xRank == 0)             mpiData.nearRanks(0) = MPI_PROC_NULL;
        if (mpiData.xRank == mpiData.npX-1) mpiData.nearRanks(1) = MPI_PROC_NULL;

        if (mpiData.yRank == 0)             mpiData.nearRanks(2) = MPI_PROC_NULL;
        if (mpiData.yRank == mpiData.npY-1) mpiData.nearRanks(3) = MPI_PROC_NULL;

    } else if (inputData.probType == "TGV") {
        // INITIALIZE PRESSURE TO 1.0 THROUGHOUT THE DOMAIN
        P->F.F = 1.0;

        // X-VELOCITY
        for (int i=V->Vx.F.F.lbound(0); i <= V->Vx.F.F.ubound(0); i++) {
            for (int k=V->Vx.F.F.lbound(2); k <= V->Vx.F.F.ubound(2); k++) {
                V->Vx.F.F(i, 0, k) = sin(2.0*M_PI*mesh.xColloc(i)/mesh.xLen)*
                                     cos(2.0*M_PI*mesh.zStaggr(k)/mesh.zLen);
            }
        }

        // Z-VELOCITY
        for (int i=V->Vz.F.F.lbound(0); i <= V->Vz.F.F.ubound(0); i++) {
            for (int k=V->Vz.F.F.lbound(2); k <= V->Vz.F.F.ubound(2); k++) {
                V->Vz.F.F(i, 0, k) = -cos(2.0*M_PI*mesh.xStaggr(i)/mesh.xLen)*
                                      sin(2.0*M_PI*mesh.zColloc(k)/mesh.zLen);
            }
        }
    }

    imposeUBCs();
    imposeWBCs();
}

void hydro2::solvePDE() {
    int xLow, xTop;
    int zLow, zTop;

    double dVol;
    double fwTime;
    double totalEnergy, localEnergy;

    int addCount;
    int seriesLength = (int)((inputData.tMax/inputData.tStp)/inputData.ioCnt);

    blitz::Array<double, 1> dSeries, eSeries, tSeries;

    if (mesh.rankData.rank == 0) {
        tSeries.resize(seriesLength + 1);
        dSeries.resize(seriesLength + 1);
        eSeries.resize(seriesLength + 1);

        addCount = 0;
    }

    sfield divV(mesh, "DIV_V", true, true, true);

    // FILE WRITING TIME
    fwTime = 0.0;

    time = 0.0;
    tStepCnt = 0;

    // PARAMETERS FOR COMPUTING TOTAL ENERGY IN THE DOMAIN
    // UPPER AND LOWER LIMITS WHEN COMPUTING ENERGY IN STAGGERED GRID
    xLow = P->F.fCore.lbound(0);        xTop = P->F.fCore.ubound(0);
    zLow = P->F.fCore.lbound(2);        zTop = P->F.fCore.ubound(2);

    // STAGGERED GRIDS HAVE SHARED POINT ACROSS MPI-SUBDOMAINS - ACCORDINGLY DECREASE LIMITS
    if (mesh.rankData.xRank > 0) {
        xLow += 1;
    }

    // INFINITESIMAL VOLUME FOR INTEGRATING ENERGY OVER DOMAIN
    dVol = hx*hz;

    // COMPUTE ENERGY AND DIVERGENCE FOR THE INITIAL CONDITION
    V->divergence(&divV);
    maxValue = divV.fieldMax();

    int iY = 0;
    localEnergy = 0.0;
    totalEnergy = 0.0;
    for (int iX = xLow; iX <= xTop; iX++) {
        for (int iZ = zLow; iZ <= zTop; iZ++) {
            localEnergy += (pow((V->Vx.F.F(iX-1, iY, iZ) + V->Vx.F.F(iX, iY, iZ))/2.0, 2.0) +
                            pow((V->Vz.F.F(iX, iY, iZ-1) + V->Vz.F.F(iX, iY, iZ))/2.0, 2.0))*dVol;
        }
    }

    MPI_Allreduce(&localEnergy, &totalEnergy, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);

    if (mesh.rankData.rank == 0) {
        std::cout << "Time: " << time << std::endl;
        std::cout << "Total energy = " << totalEnergy << std::endl;
        std::cout << "Maximum value of divergence = " << maxValue << std::endl;
        std::cout << std::endl;

        tSeries(addCount) = time;
        eSeries(addCount) = totalEnergy;
        dSeries(addCount) = maxValue;
        addCount += 1;
    }

    // TIME-INTEGRATION LOOP
    while (true) {
        if (std::abs(fwTime - time) < 0.5*inputData.tStp) {
            dataWriter->writeHDF5(time);
            fwTime += inputData.fwInt;
        }

        // MAIN FUNCTION CALLED IN EACH LOOP TO UPDATE THE FIELDS AT EACH TIME-STEP
        implicitCN();

        tStepCnt += 1;
        time += inputData.tStp;

        V->divergence(&divV);
        maxValue = divV.fieldMax();

        int iY = 0;
        localEnergy = 0.0;
        totalEnergy = 0.0;
        for (int iX = xLow; iX <= xTop; iX++) {
            for (int iZ = zLow; iZ <= zTop; iZ++) {
                localEnergy += (pow((V->Vx.F.F(iX-1, iY, iZ) + V->Vx.F.F(iX, iY, iZ))/2.0, 2.0) +
                                pow((V->Vz.F.F(iX, iY, iZ-1) + V->Vz.F.F(iX, iY, iZ))/2.0, 2.0))*dVol;
            }
        }

        MPI_Allreduce(&localEnergy, &totalEnergy, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);

        if (tStepCnt % inputData.ioCnt == 0) {
            if (mesh.rankData.rank == 0) {
                std::cout << "Time: " << time << std::endl;
                std::cout << "Total energy = " << totalEnergy << std::endl;
                std::cout << "Maximum value of divergence = " << maxValue << std::endl;
                std::cout << std::endl;

                tSeries(addCount) = time;
                eSeries(addCount) = totalEnergy;
                dSeries(addCount) = maxValue;
                addCount += 1;
            }
        }

        if (time > inputData.tMax) {
            break;
        }
    }

    // WRITE THE TIME SERIES OF ENERGY AND DIVERGENCE INTO FILE
    if (mesh.rankData.rank == 0) {
        std::ofstream ofFile("output/TimeSeries.dat");
        ofFile << "VARIABLES = time, energy, divergence\n"
               << "ZONE T=S\tI = " << seriesLength + 1 <<
                          "\tJ = " << 1 <<
                          "\tK = " << 1 <<
                          "\tF = POINT\n";
        for (int i=0; i<=seriesLength; i++) {
            ofFile << std::fixed << std::setw(6)  << std::setprecision(4) << tSeries(i) << "\t" <<
                                    std::setw(16) << std::setprecision(8) << eSeries(i) << "\t" <<
                                                                             dSeries(i) << std::endl;
        }
        ofFile.close();
    }
}

void hydro2::implicitCN() {
    *H = 0.0;

    // CALCULATE H FROM THE NON LINEAR TERMS AND HALF THE VISCOUS TERMS
    V->computeViscous(H);

    // TEMPORARILY WRITING THE CONVECTIVE DERIVATIVE INTO THE gradPres FIELD
    V->computeNLin(*V, &gradPres);
    *H -= gradPres;

    gradPres = 0.0;
    P->gradient(gradPres);

    // ADD PRESSURE GRADIENT TO NON-LINEAR TERMS AND MULTIPLY WITH TIME-STEP
    *H -= gradPres;
    *H *= inputData.tStp;

    // ADD THE CALCULATED VALUES TO THE VELOCITY AT START OF TIME-STEP
    *H += *V;

    // SYNCHRONISE THE RHS OF TIME INTEGRATION STEP THUS OBTAINED ACROSS ALL PROCESSORS
    H->syncData();

    // CALCULATE V IMPLICITLY USING THE JACOBI ITERATIVE SOLVER
    uSolve();
    wSolve();

    if (tStepCnt % inputData.ioCnt == 0) {
        if (mesh.rankData.rank == 0) {
            std::cout << std::endl;
        }
    }

    // CALCULATE THE RHS FOR THE POISSON SOLVER FROM THE GUESSED VALUES OF VELOCITY IN V
    V->divergence(&mgRHS);
    mgRHS *= 1.0/inputData.tStp;

    // USING THE CALCULATED mgRHS, EVALUATE Pp USING MULTI-GRID METHOD
    //mgSolver->mgSolve(Pp, mgRHS);

    // SYNCHRONISE THE PRESSURE CORRECTION ACROSS PROCESSORS
    Pp.mpiHandle->syncData();

    // ADD THE PRESSURE CORRECTION CALCULATED FROM THE POISSON SOLVER TO P
    *P += Pp;

    // CALCULATE FINAL VALUE OF V BY SUBTRACTING THE GRADIENT OF PRESSURE CORRECTION
    Pp.gradient(gradPres);
    gradPres *= inputData.tStp;
    *V -= gradPres;

    // IMPOSE BOUNDARY CONDITIONS ON V
    imposeUBCs();
    imposeWBCs();
}

void hydro2::uSolve() {
    blitz::firstIndex i;
    blitz::thirdIndex k;

    int iterCount = 0;

    xCoreRange = blitz::Range(V->Vx.F.fBulk.lbound(0), V->Vx.F.fBulk.ubound(0));
    zCoreRange = blitz::Range(V->Vx.F.fBulk.lbound(2), V->Vx.F.fBulk.ubound(2));

    while (true) {
        jacGuess.Vx.F.F(V->Vx.F.fBulk) = ((hz2 * mesh.xix2Colloc(xCoreRange)(i) * (V->Vx.F.F(V->Vx.F.fBRgt) + V->Vx.F.F(V->Vx.F.fBLft)) +
                                           hx2 * mesh.ztz2Staggr(zCoreRange)(k) * (V->Vx.F.F(V->Vx.F.fBTop) + V->Vx.F.F(V->Vx.F.fBBot))) *
                        inputData.tStp / ( hzhx * 2.0 * inputData.Re) + H->Vx.F.F(V->Vx.F.fBulk))/
                 (1.0 + inputData.tStp * ((hz2 * mesh.xix2Colloc(xCoreRange)(i) +
                                           hx2 * mesh.ztz2Staggr(zCoreRange)(k)))/(inputData.Re * hzhx));

        V->Vx.F.F = jacGuess.Vx.F.F;

        imposeUBCs();

        lapField.Vx.F.F(V->Vx.F.fBulk) = V->Vx.F.F(V->Vx.F.fBulk) - (
            mesh.xix2Colloc(xCoreRange)(i) * (V->Vx.F.F(V->Vx.F.fBRgt) - 2.0 * V->Vx.F.F(V->Vx.F.fBulk) + V->Vx.F.F(V->Vx.F.fBLft)) / (hx2) +
            mesh.ztz2Staggr(zCoreRange)(k) * (V->Vx.F.F(V->Vx.F.fBTop) - 2.0 * V->Vx.F.F(V->Vx.F.fBulk) + V->Vx.F.F(V->Vx.F.fBBot)) / (hz2)) *
                                       0.5 * inputData.tStp / inputData.Re;

        lapField.Vx.F.F(V->Vx.F.fBulk) = abs(lapField.Vx.F.F(V->Vx.F.fBulk) - H->Vx.F.F(V->Vx.F.fBulk));

        maxValue = lapField.Vx.fieldMax();

        if (maxValue < inputData.tolerance) {
            if (tStepCnt % inputData.ioCnt == 0) {
                if (mesh.rankData.rank == 0) {
                    std::cout << "\033[32mJacobi solver for U converged in " << iterCount << " iterations\033[0m" << std::endl;
                }
            }
            break;
        }

        iterCount += 1;

        if (iterCount > maxCount) {
            if (mesh.rankData.rank == 0) {
                std::cout << "\033[31mERROR:\033[0m Jacobi iterations for solution at coarsest level not converging. Aborting" << std::endl;
                exit(0);
            }
            break;
        }
    }
}

void hydro2::wSolve() {
    blitz::firstIndex i;
    blitz::thirdIndex k;

    int iterCount = 0;

    xCoreRange = blitz::Range(V->Vz.F.fBulk.lbound(0), V->Vz.F.fBulk.ubound(0));
    zCoreRange = blitz::Range(V->Vz.F.fBulk.lbound(2), V->Vz.F.fBulk.ubound(2));

    while (true) {
        jacGuess.Vz.F.F(V->Vz.F.fBulk) = ((hz2 * mesh.xix2Staggr(xCoreRange)(i) * (V->Vz.F.F(V->Vz.F.fBRgt) + V->Vz.F.F(V->Vz.F.fBLft)) +
                                           hx2 * mesh.ztz2Colloc(zCoreRange)(k) * (V->Vz.F.F(V->Vz.F.fBTop) + V->Vz.F.F(V->Vz.F.fBBot))) *
                        inputData.tStp / ( hzhx * 2.0 * inputData.Re) + H->Vz.F.F(V->Vz.F.fBulk))/
                 (1.0 + inputData.tStp * ((hz2 * mesh.xix2Staggr(xCoreRange)(i) +
                                           hx2 * mesh.ztz2Colloc(zCoreRange)(k)))/(inputData.Re * hzhx));

        V->Vz.F.F = jacGuess.Vz.F.F;

        imposeWBCs();

        lapField.Vz.F.F(V->Vz.F.fBulk) = V->Vz.F.F(V->Vz.F.fBulk) - (
            mesh.xix2Staggr(xCoreRange)(i) * (V->Vz.F.F(V->Vz.F.fBRgt) - 2.0 * V->Vz.F.F(V->Vz.F.fBulk) + V->Vz.F.F(V->Vz.F.fBLft)) / (hx2) +
            mesh.ztz2Colloc(zCoreRange)(k) * (V->Vz.F.F(V->Vz.F.fBTop) - 2.0 * V->Vz.F.F(V->Vz.F.fBulk) + V->Vz.F.F(V->Vz.F.fBBot)) / (hz2)) *
                                       0.5 * inputData.tStp / inputData.Re;

        lapField.Vz.F.F(V->Vz.F.fBulk) = abs(lapField.Vz.F.F(V->Vz.F.fBulk) - H->Vz.F.F(V->Vz.F.fBulk));

        maxValue = lapField.Vz.fieldMax();

        if (maxValue < inputData.tolerance) {
            if (tStepCnt % inputData.ioCnt == 0) {
                if (mesh.rankData.rank == 0) {
                    std::cout << "\033[32mJacobi solver for W converged in " << iterCount << " iterations\033[0m" << std::endl;
                }
            }
            break;
        }

        iterCount += 1;

        if (iterCount > maxCount) {
            if (mesh.rankData.rank == 0) {
                std::cout << "\033[31mERROR:\033[0m Jacobi iterations for solution at coarsest level not converging. Aborting" << std::endl;
                exit(0);
            }
            break;
        }
    }
}

void hydro2::setCoefficients() {
    hx = mesh.dXi;
    hz = mesh.dZt;

    hx2 = pow(mesh.dXi, 2.0);
    hz2 = pow(mesh.dZt, 2.0);

    hzhx = pow(mesh.dZt, 2.0)*pow(mesh.dXi, 2.0);
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to impose global and sub-domain boundary values on x-velocity component
 *
 *          First, inter-processor data transfer is performed by calling the \ref mpidata#syncData "syncData" function of the
 *          \ref sfield#mpiHandle "mpiHandle" of the <B>Vx</B> component.
 *          Then the values of <B>Vx</B> on the 4 walls are imposed.
 *          The order of imposing boundary conditions is - left, right, bottom and top boundaries.
 *          The corner values are not being imposed specifically and is thus dependent on the above order.
 ********************************************************************************************************************************************
 */
void hydro2::imposeUBCs() {
    V->Vx.mpiHandle->syncData();

    if (inputData.probType == "LDC") {
        // Vx LIES ON EITHER SIDE OF THE LEFT WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS COLLOCATED ALONG X
        if (mesh.rankData.xRank == 0) {
            V->Vx.F.F(V->Vx.F.fWalls(0)) = -V->Vx.F.F(V->Vx.F.xDeriv->shift(V->Vx.F.fWalls(0), 1));
        }
        // Vx LIES ON EITHER SIDE OF THE RIGHT WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS COLLOCATED ALONG X
        if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
            V->Vx.F.F(V->Vx.F.fWalls(1)) = -V->Vx.F.F(V->Vx.F.xDeriv->shift(V->Vx.F.fWalls(1), -1));
        }

        // Vx LIES ON THE BOTTOM WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Z
        V->Vx.F.F(V->Vx.F.fWalls(4)) = 0.0;
        // Vx LIES ON THE TOP WALL AS THE WALL IS ON STAGGERED POINT AND Vx IS STAGGERED ALONG Z
        V->Vx.F.F(V->Vx.F.fWalls(5)) = 1.0;

    } else if (inputData.probType == "TGV") {
        V->Vx.F.F(V->Vx.F.fWalls(4)) = V->Vx.F.F(V->Vx.F.zDeriv->shift(V->Vx.F.fWalls(5), -2));
        V->Vx.F.F(V->Vx.F.fWalls(5)) = V->Vx.F.F(V->Vx.F.zDeriv->shift(V->Vx.F.fWalls(4), 2));
    }
}

/**
 ********************************************************************************************************************************************
 * \brief   Function to impose global and sub-domain boundary values on z-velocity component
 *
 *          First, inter-processor data transfer is performed by calling the \ref mpidata#syncData "syncData" function of the
 *          \ref sfield#mpiHandle "mpiHandle" of the <B>Vz</B> component.
 *          Then the values of <B>Vz</B> on the 4 walls are imposed.
 *          The order of imposing boundary conditions is - left, right, bottom and top boundaries.
 *          The corner values are not being imposed specifically and is thus dependent on the above order.
 ********************************************************************************************************************************************
 */
void hydro2::imposeWBCs() {
    V->Vz.mpiHandle->syncData();

    if (inputData.probType == "LDC") {
        // Vz LIES ON THE LEFT WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG X
        if (mesh.rankData.xRank == 0) {
            V->Vz.F.F(V->Vz.F.fWalls(0)) = 0.0;
        }
        // Vz LIES ON THE RIGHT WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS STAGGERED ALONG X
        if (mesh.rankData.xRank == mesh.rankData.npX - 1) {
            V->Vz.F.F(V->Vz.F.fWalls(1)) = 0.0;
        }

        // Vz LIES ON EITHER SIDE OF THE BOTTOM WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS COLLOCATED ALONG Z
        V->Vz.F.F(V->Vz.F.fWalls(4)) = -V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(4), 1));
        // Vz LIES ON EITHER SIDE OF THE TOP WALL AS THE WALL IS ON STAGGERED POINT AND Vz IS COLLOCATED ALONG Z
        V->Vz.F.F(V->Vz.F.fWalls(5)) = -V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(5), -1));

    } else if (inputData.probType == "TGV") {
        V->Vz.F.F(V->Vz.F.fWalls(4)) = V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(5), -1));
        V->Vz.F.F(V->Vz.F.fWalls(5)) = V->Vz.F.F(V->Vz.F.zDeriv->shift(V->Vz.F.fWalls(4), 1));
    }
}

hydro2::~hydro2() {
    delete P;
    delete V;

    delete H;

    delete dataWriter;
}
